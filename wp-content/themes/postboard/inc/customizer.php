<?php
/**
 * Register custom customizer panels, sections and settings.
 *
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

/**
 * We register our custom customizer by using the hook.
 *
 * @since  1.0.0
 */
function postboard_customizer_register() {

	// Stores all the controls that will be added
	$options = array();

	// Stores all the sections to be added
	$sections = array();

	// Stores all the panels to be added
	$panels = array();

	// Adds the sections to the $options array
	$options['sections'] = $sections;

	// ======= Start Customizer Panels/Sections/Settings ======= //
	
	// General Panels and Sections
	$general_panel = 'general';

	$panels[] = array(
		'id'          => $general_panel,
		'title'       => __( 'General', 'postboard' ),
		'description' => __( 'This panel is used for managing general section of your site.', 'postboard' ),
		'priority'    => 10
	);

		// RSS
		$section = PREFIX . 'rss-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'RSS', 'postboard' ),
			'priority'    => 100,
			'panel'       => $general_panel,
			'description' => __( 'If you fill the custom rss url below, it will replace the default.', 'postboard' ),
		);
		$options[PREFIX . 'custom-rss'] = array(
			'id'           => PREFIX . 'custom-rss',
			'label'        => __( 'Custom RSS URL (eg. Feedburner)', 'postboard' ),
			'section'      => $section,
			'type'         => 'url',
			'default'      => ''
		);

		// Comment
		$section = PREFIX . 'comment-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Comments', 'postboard' ),
			'priority'    => 110,
			'panel'       => $general_panel,
		);
		$options[PREFIX . 'page-comment'] = array(
			'id'           => PREFIX . 'page-comment',
			'label'        => __( 'Page Comment', 'postboard' ),
			'section'      => $section,
			'type'         => 'switch',
			'default'      => 1
		);

		// Comment
		$section = PREFIX . 'post-date-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Post Date Style', 'postboard' ),
			'priority'    => 115,
			'panel'       => $general_panel,
		);
		$options[PREFIX . 'post-date-style'] = array(
			'id'          => PREFIX . 'post-date-style',
			'label'       => __( 'Style', 'postboard' ),
			'section'     => $section,
			'type'        => 'select',
			'default'     => 'absolute',
			'choices'     => array(
				'absolute' => __( 'Absolute (June 20, 2015 10:19 am)', 'postboard' ),
				'relative' => __( 'Relative (1 week ago)', 'postboard' )
			)
		);

		// Social header
		$section = PREFIX . 'social-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Social', 'postboard' ),
			'priority'    => 120,
			'panel'       => $general_panel
		);
		$options[PREFIX . 'twitter'] = array(
			'id'      => PREFIX . 'twitter',
			'label'   => __( 'Twitter URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);
		$options[PREFIX . 'facebook'] = array(
			'id'      => PREFIX . 'facebook',
			'label'   => __( 'Facebook URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);
		$options[PREFIX . 'gplus'] = array(
			'id'      => PREFIX . 'gplus',
			'label'   => __( 'Google Plus URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);
		$options[PREFIX . 'instagram'] = array(
			'id'      => PREFIX . 'instagram',
			'label'   => __( 'Instagram URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);
		$options[PREFIX . 'pinterest'] = array(
			'id'      => PREFIX . 'pinterest',
			'label'   => __( 'Pinterest URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);
		$options[PREFIX . 'linkedin'] = array(
			'id'      => PREFIX . 'linkedin',
			'label'   => __( 'LinkedIn URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);
		$options[PREFIX . 'tumblr'] = array(
			'id'      => PREFIX . 'tumblr',
			'label'   => __( 'Tumblr URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);
		$options[PREFIX . 'rss'] = array(
			'id'      => PREFIX . 'rss',
			'label'   => __( 'RSS URL', 'postboard' ),
			'section' => $section,
			'type'    => 'url',
			'default' => ''
		);

		// Footer Text
		$section = PREFIX . 'footer-text-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Footer Text', 'postboard' ),
			'priority'    => 130,
			'panel'       => $general_panel,
		);
		$theme = wp_get_theme();
		$options[PREFIX . 'footer-text'] = array(
			'id'           => PREFIX . 'footer-text',
			'label'        => '',
			'description'  => __( 'Customize the footer text.', 'postboard' ),
			'section'      => $section,
			'type'         => 'textarea',
			'default'      => sprintf( __( '&copy; %1$s %2$s &middot; %3$s %4$s &middot; Designed by %5$s', 'postboard' ), 
				date( 'Y' ),
				'<a href="' . esc_url( home_url() ) . '">' . esc_attr( get_bloginfo( 'name' ) ) . '</a>',
				'<span class="themejunkie">',
				'<a href="' . esc_url( $theme->get( 'ThemeURI' ) ) . '">' . esc_attr( $theme->get( 'Name' ) ) . '</a>',
				'<a href="' . esc_url( $theme->get( 'AuthorURI' ) ) . '">' . esc_attr( $theme->get( 'Author' ) ) . '</a>.</span>' )
		);

	// Header Panels and Sections
	$header_panel = 'header';

	$panels[] = array(
		'id'          => $header_panel,
		'title'       => __( 'Header', 'postboard' ),
		'description' => __( 'This panel is used for managing header section of your site.', 'postboard' ),
		'priority'    => 15
	);

		// Logo
		$section = PREFIX . 'logo-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Logo', 'postboard' ),
			'priority'    => 30,
			'panel'       => $header_panel
		);
		$options[PREFIX . 'logo'] = array(
			'id'      => PREFIX . 'logo',
			'label'   => __( 'Regular Logo', 'postboard' ),
			'section' => $section,
			'type'    => 'media',
			'default' => ''
		);

		// Header Style
		$section = PREFIX . 'header-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Header Style', 'postboard' ),
			'description' => __( 'Choose header style', 'postboard' ),
			'priority'    => 35,
			'panel'       => $header_panel
		);
		$options[PREFIX . 'header-style'] = array(
			'id'          => PREFIX . 'header-style',
			'label'       => __( 'Header Style', 'postboard' ),
			'section'     => $section,
			'type'        => 'radio',
			'default'     => 'header',
			'choices'     => array(
				'header'   => __( 'Center', 'postboard' ),
				'header2'  => __( 'With Ads', 'postboard' )
			)
		);

		// Secondary Menu Style
		$section = PREFIX . 'menu-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Secondary Menu Style', 'postboard' ),
			'description' => __( 'Choose secondary menu style', 'postboard' ),
			'priority'    => 35,
			'panel'       => $header_panel
		);
		$options[PREFIX . 'menu-style'] = array(
			'id'          => PREFIX . 'menu-style',
			'label'       => __( 'Secondary Menu Style', 'postboard' ),
			'section'     => $section,
			'type'        => 'radio',
			'default'     => 'secondary-bar1',
			'choices'     => array(
				'secondary-bar1'  => __( 'With Search Form', 'postboard' ),
				'secondary-bar2'  => __( 'Center without Search Form', 'postboard' )
			)
		);

		// Search icon and form
		$section = PREFIX . 'search-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Search', 'postboard' ),
			'description' => __( 'Show search icon and form', 'postboard' ),
			'priority'    => 40,
			'panel'       => $header_panel
		);
		$options[PREFIX . 'search-header'] = array(
			'id'      => PREFIX . 'search-header',
			'label'   => '',
			'section' => $section,
			'type'    => 'switch',
			'default' => 1
		);

		// Loop Title
		$section = PREFIX . 'title-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Title', 'postboard' ),
			'description' => __( 'Display Title and Subtitle', 'postboard' ),
			'priority'    => 50,
			'panel'       => $header_panel
		);
		$options[PREFIX . 'loop-title'] = array(
			'id'          => PREFIX . 'loop-title',
			'label'       => __( 'Title', 'postboard' ),
			'section'     => $section,
			'type'        => 'text',
			'default'     => __( "What's New?", 'postboard' )
		);
		$options[PREFIX . 'loop-description'] = array(
			'id'          => PREFIX . 'loop-description',
			'label'       => __( 'Subtitle/Description', 'postboard' ),
			'section'     => $section,
			'type'        => 'textarea',
			'default'     => __( 'Browse our latest posts below or <a href="#">subscribe</a> to our newsletter', 'postboard' )
		);

	// Colors Panel and Sections
	$color_panel = 'color';

	$panels[] = array(
		'id'          => $color_panel,
		'title'       => __( 'Color', 'postboard' ),
		'description' => __( 'This panel is used for managing colors of your site.', 'postboard' ),
		'priority'    => 20
	);
		
		// Header background color
		$section = PREFIX . 'header-bg-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Header', 'postboard' ),
			'priority'    => 5,
			'panel'       => $color_panel
		);
		// $options[PREFIX . 'header-color'] = array(
		// 	'id'          => PREFIX . 'header-color',
		// 	'label'       => __( 'Background color', 'postboard' ),
		// 	'section'     => $section,
		// 	'type'        => 'color',
		// 	'default'     => '#141414',
		// 	'transport'   => 'postMessage'
		// );
		$options[PREFIX . 'site-title-color'] = array(
			'id'          => PREFIX . 'site-title-color',
			'label'       => __( 'Site Title', 'postboard' ),
			'section'     => $section,
			'type'        => 'color',
			'default'     => '#333333',
			'transport'   => 'postMessage'
		);

	// Typography Panel and Sections
	$typo_panel = 'typography';

	$panels[] = array(
		'id'          => $typo_panel,
		'title'       => __( 'Typography', 'postboard' ),
		'description' => __( 'This panel is used for managing typography of your site.', 'postboard' ),
		'priority'    => 30
	);

		// Global typography
		$section = PREFIX . 'global-typography';
		$font_choices = customizer_library_get_font_choices();

		$sections[] = array(
			'id'       => $section,
			'title'    => __( 'Global', 'postboard' ),
			'priority' => 5,
			'panel'    => $typo_panel
		);
		$options[PREFIX . 'text-font'] = array(
			'id'          => PREFIX . 'text-font',
			'label'       => __( 'Text font', 'postboard' ),
			'section'     => $section,
			'type'        => 'select2',
			'choices'     => $font_choices,
			'default'     => 'Roboto',
		);
		$options[PREFIX . 'heading-font'] = array(
			'id'          => PREFIX . 'heading-font',
			'label'       => __( 'Heading font', 'postboard' ),
			'section'     => $section,
			'type'        => 'select2',
			'choices'     => $font_choices,
			'default'     => 'Roboto',
		);

	// Content Panel and Sections
	$content_panel = 'layouts';

	$panels[] = array(
		'id'          => $content_panel,
		'title'       => __( 'Layouts', 'postboard' ),
		'description' => __( 'This panel is used for managing several custom features/layouts of your site.', 'postboard' ),
		'priority'    => 35
	);

		// Featured posts
		$section = PREFIX . 'featured-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Featured Posts', 'postboard' ),
			'priority'    => 10,
			'panel'       => $content_panel
		);
		$options[PREFIX . 'featured-enable'] = array(
			'id'          => PREFIX . 'featured-enable',
			'label'       => __( 'Show featured posts', 'postboard' ),
			'section'     => $section,
			'type'        => 'switch',
			'default'     => 1
		);
		$options[PREFIX . 'featured-tag'] = array(
			'id'          => PREFIX . 'featured-tag',
			'label'       => __( 'Select a tag', 'postboard' ),
			'section'     => $section,
			'type'        => 'select2',
			'choices'     => postboard_tags_list()
		);
		$options[PREFIX . 'featured-num'] = array(
			'id'          => PREFIX . 'featured-num',
			'label'       => __( 'Number of posts', 'postboard' ),
			'section'     => $section,
			'type'        => 'text',
			'default'     => 6
		);
		$options[PREFIX . 'featured-orderby'] = array(
			'id'          => PREFIX . 'featured-orderby',
			'label'       => __( 'Order by', 'postboard' ),
			'section'     => $section,
			'type'        => 'select',
			'default'     => 'date',
			'choices'     => array(
				'date'  => __( 'Date', 'postboard' ),
				'rand'  => __( 'Random', 'postboard' )
			)
		);

		// Archive layout
		$section = PREFIX . 'archive-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Archive', 'postboard' ),
			'description' => __( 'Applied to index/archive pages including category, tag, author and search pages', 'postboard' ),
			'priority'    => 20,
			'panel'       => $content_panel
		);
		$options[PREFIX . 'archive-layout'] = array(
			'id'          => PREFIX . 'archive-layout',
			'label'       => __( 'Archive Layout', 'postboard' ),
			'section'     => $section,
			'type'        => 'radio',
			'default'     => 'three-columns',
			'choices'     => array(
				'four-columns'  => __( '4 Columns', 'postboard' ),
				'three-columns' => __( '3 Columns', 'postboard' ),
				'two-columns'   => __( '2 Columns', 'postboard' ),
				'one-column'    => __( '1 Column', 'postboard' )
			)
		);
		$options[PREFIX . 'archive-width'] = array(
			'id'          => PREFIX . 'archive-width',
			'label'       => __( 'Grid Width', 'postboard' ),
			'description' => __( 'Allow custom width per post', 'postboard' ),
			'section'     => $section,
			'type'        => 'radio',
			'default'     => 'default',
			'choices'     => array(
				'default' => __( 'Default', 'postboard' ),
				'width2'  => __( 'Custom', 'postboard' ),
			)
		);
		$options[PREFIX . 'archive-nav'] = array(
			'id'          => PREFIX . 'archive-nav',
			'label'       => __( 'Use Infinite Scroll', 'postboard' ),
			'section'     => $section,
			'type'        => 'switch',
			'default'     => 1
		);

		// Posts
		$section = PREFIX . 'posts-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Posts', 'postboard' ),
			'description' => __( 'Posts is a single post page.', 'postboard' ),
			'priority'    => 30,
			'panel'       => $content_panel
		);
		$options[PREFIX . 'post-meta-group'] = array(
			'id'          => PREFIX . 'post-meta-group',
			'label'       => __( 'Post Header', 'postboard' ),
			'section'     => $section,
			'type'        => 'group-title'
		);
			$options[PREFIX . 'post-thumbnail'] = array(
				'id'          => PREFIX . 'post-thumbnail',
				'label'       => __( 'Show featured image', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'post-date'] = array(
				'id'          => PREFIX . 'post-date',
				'label'       => __( 'Show post date', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'post-author'] = array(
				'id'          => PREFIX . 'post-author',
				'label'       => __( 'Show post author name', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'post-like'] = array(
				'id'          => PREFIX . 'post-like',
				'label'       => __( 'Show post like', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'post-comment'] = array(
				'id'          => PREFIX . 'post-comment',
				'label'       => __( 'Show comment count', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
		$options[PREFIX . 'post-footer-group'] = array(
			'id'          => PREFIX . 'post-footer-group',
			'label'       => __( 'Post Footer', 'postboard' ),
			'section'     => $section,
			'type'        => 'group-title'
		);
			$options[PREFIX . 'post-cat'] = array(
				'id'          => PREFIX . 'post-cat',
				'label'       => __( 'Show post categories', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'post-tag'] = array(
				'id'          => PREFIX . 'post-tag',
				'label'       => __( 'Show post tags', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'post-share'] = array(
				'id'          => PREFIX . 'post-share',
				'label'       => __( 'Show post share', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'post-nav'] = array(
				'id'          => PREFIX . 'post-nav',
				'label'       => __( 'Show post navigation', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);
			$options[PREFIX . 'related-posts'] = array(
				'id'          => PREFIX . 'related-posts',
				'label'       => __( 'Show author box and related posts', 'postboard' ),
				'section'     => $section,
				'type'        => 'switch',
				'default'     => 1
			);

		// Page
		$section = PREFIX . 'page-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Page', 'postboard' ),
			'priority'    => 40,
			'panel'       => $content_panel
		);
		$options[PREFIX . 'page-title'] = array(
			'id'          => PREFIX . 'page-title',
			'label'       => __( 'Show page title', 'postboard' ),
			'section'     => $section,
			'type'        => 'switch',
			'default'     => 1
		);

	// Advertisement Panel and Sections
	$ads_panel = 'advertisement';

	$panels[] = array(
		'id'       => $ads_panel,
		'title'    => __( 'Advertisement', 'postboard' ),
		'priority' => 45
	);

		// Header ads
		$section = PREFIX . 'header-ads-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Header', 'postboard' ),
			'priority'    => 5,
			'panel'       => $ads_panel,
		);
		$options[PREFIX . 'header-ads-image'] = array(
			'id'           => PREFIX . 'header-ads-image',
			'label'        => __( 'Ads Image', 'postboard' ),
			'description'  => __( 'Upload your ads image then put the url in the setting below.', 'postboard' ),
			'section'      => $section,
			'type'         => 'media',
			'default'      => '',
		);
		$options[PREFIX . 'header-ads-url'] = array(
			'id'           => PREFIX . 'header-ads-url',
			'label'        => __( 'Ads URL', 'postboard' ),
			'description'  => __( 'Put the ads url in the box below.', 'postboard' ),
			'section'      => $section,
			'type'         => 'url',
			'default'      => ''
		);
		$options[PREFIX . 'header-ads-custom'] = array(
			'id'                => PREFIX . 'header-ads-custom',
			'label'             => __( 'Or', 'postboard' ),
			'description'       => __( 'Pur you custom ads code (eg. adsense) in the box below.', 'postboard' ),
			'section'           => $section,
			'type'              => 'textarea',
			'sanitize_callback' => 'postboard_textarea_stripslashes',
			'default'           => ''
		);

		// Posts ads
		$section = PREFIX . 'posts-ads-section';

		$sections[] = array(
			'id'          => $section,
			'title'       => __( 'Posts', 'postboard' ),
			'description' => __( 'Single post advertisement', 'postboard' ),
			'priority'    => 10,
			'panel'       => $ads_panel,
		);
		$options[PREFIX . 'post-ads-before'] = array(
			'id'          => PREFIX . 'post-ads-before',
			'label'       => __( 'Ads Before Content', 'postboard' ),
			'section'     => $section,
			'type'        => 'group-title'
		);
			$options[PREFIX . 'post-ads-image-before'] = array(
				'id'           => PREFIX . 'post-ads-image-before',
				'label'        => __( 'Ads Image', 'postboard' ),
				'description'  => __( 'Upload your ads image then put the url in the setting below.', 'postboard' ),
				'section'      => $section,
				'type'         => 'media',
				'default'      => '',
			);
			$options[PREFIX . 'post-ads-url-before'] = array(
				'id'           => PREFIX . 'post-ads-url-before',
				'label'        => __( 'Ads URL', 'postboard' ),
				'description'  => __( 'Put the ads url in the box below.', 'postboard' ),
				'section'      => $section,
				'type'         => 'url',
				'default'      => ''
			);
			$options[PREFIX . 'post-ads-custom-before'] = array(
				'id'                => PREFIX . 'post-ads-custom-before',
				'label'             => __( 'Or', 'postboard' ),
				'description'       => __( 'Pur you custom ads code (eg. adsense) in the box below.', 'postboard' ),
				'section'           => $section,
				'type'              => 'textarea',
				'sanitize_callback' => 'postboard_textarea_stripslashes',
				'default'           => ''
			);
		$options[PREFIX . 'post-ads-after'] = array(
			'id'          => PREFIX . 'post-ads-after',
			'label'       => __( 'Ads After Content', 'postboard' ),
			'section'     => $section,
			'type'        => 'group-title'
		);
			$options[PREFIX . 'post-ads-image-after'] = array(
				'id'           => PREFIX . 'post-ads-image-after',
				'label'        => __( 'Ads Image', 'postboard' ),
				'description'  => __( 'Upload your ads image then put the url in the setting below.', 'postboard' ),
				'section'      => $section,
				'type'         => 'media',
				'default'      => '',
			);
			$options[PREFIX . 'post-ads-url-after'] = array(
				'id'           => PREFIX . 'post-ads-url-after',
				'label'        => __( 'Ads URL', 'postboard' ),
				'description'  => __( 'Put the ads url in the box below.', 'postboard' ),
				'section'      => $section,
				'type'         => 'url',
				'default'      => ''
			);
			$options[PREFIX . 'post-ads-custom-after'] = array(
				'id'                => PREFIX . 'post-ads-custom-after',
				'label'             => __( 'Or', 'postboard' ),
				'description'       => __( 'Pur you custom ads code (eg. adsense) in the box below.', 'postboard' ),
				'section'           => $section,
				'type'              => 'textarea',
				'sanitize_callback' => 'postboard_textarea_stripslashes',
				'default'           => ''
			);

	// Adds the sections to the $options array
	$options['sections'] = $sections;

	// Adds the panels to the $options array
	$options['panels'] = $panels;

	$customizer_library = Customizer_Library::Instance();
	$customizer_library->add_options( $options );

}
add_action( 'init', 'postboard_customizer_register' );