<?php
/**
 * Meta boxes functions
 *
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

/* Register meta boxes. */
add_action( 'add_meta_boxes', 'postboard_add_meta_boxes' );

/* Save meta boxes. */
add_action( 'save_post', 'postboard_meta_boxes_save', 10, 2 );

/**
 * Registers new meta boxes for the 'team_item' post editing screen in the admin.
 *
 * @since  0.1.0
 * @access public
 * @link   http://codex.wordpress.org/Function_Reference/add_meta_box
 */
function postboard_add_meta_boxes( $post ) {

	/* Check if current screen is team page. */
	if ( 'post' != get_current_screen()->post_type )
		return;

	global $post;

	if ( current_user_can( 'edit_post_meta', $post->ID ) || current_user_can( 'add_post_meta', $post->ID ) || current_user_can( 'delete_post_meta', $post->ID ) )
		add_meta_box( 
			'postboard-metaboxes-post',
			__( 'Grid Width', 'postboard' ),
			'postboard_metaboxes_display',
			'post',
			'side',
			'core'
		);

}

/**
 * Displays the content of the meta boxes.
 *
 * @param  object  $post
 * @since  0.1.0
 * @access public
 */
function postboard_metaboxes_display( $post ) {

	/* Get the current post's width */
	$post_width = get_post_meta( $post->ID, 'postboard_post_width', true );

	$post_width = ( $post_width ) ? $post_width : 'default';

	wp_nonce_field( basename( __FILE__ ), 'postboard-metaboxes-post-nonce' ); ?>

	<p class="howto"><?php _e( 'How this post be displayed in archive grid', 'postboard' ); ?></p>

	<div class="post-width">

		<div class="post-width-wrap">
			<ul>
				<li><input type="radio" name="post-width" id="post-width-default" value="default" <?php checked( $post_width, 'default' );?> /> <label for="post-width-default"><?php echo esc_html( 'Default', 'postboard' ); ?></label></li>
				<li><input type="radio" name="post-width" id="post-width-width2" value="grid-item--width2" <?php checked( $post_width, 'grid-item--width2' );?> /> <label for="post-width-width2"><?php echo esc_html__( 'Wide', 'postboard' ); ?></label></li>
			</ul>
		</div>

	</div><!-- .post-width -->

	<?php
}

/**
 * Saves the metadata for the team item info meta box.
 *
 * @param  int     $post_id
 * @param  object  $post
 * @since  0.1.0
 * @access public
 */
function postboard_meta_boxes_save( $post_id, $post ) {

	if ( ! isset( $_POST['postboard-metaboxes-post-nonce'] ) || ! wp_verify_nonce( $_POST['postboard-metaboxes-post-nonce'], basename( __FILE__ ) ) )
		return;

	if ( ! current_user_can( 'edit_post', $post_id ) )
		return;

	$meta = array(
		'postboard_post_width' => esc_attr( $_POST['post-width'] )
	);

	foreach ( $meta as $meta_key => $new_meta_value ) {

		/* Get the meta value of the custom field key. */
		$meta_value = get_post_meta( $post_id, $meta_key, true );

		/* If there is no new meta value but an old value exists, delete it. */
		if ( current_user_can( 'delete_post_meta', $post_id, $meta_key ) && '' == $new_meta_value && $meta_value )
			delete_post_meta( $post_id, $meta_key, $meta_value );

		/* If a new meta value was added and there was no previous value, add it. */
		elseif ( current_user_can( 'add_post_meta', $post_id, $meta_key ) && $new_meta_value && '' == $meta_value )
			add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		/* If the new meta value does not match the old value, update it. */
		elseif ( current_user_can( 'edit_post_meta', $post_id, $meta_key ) && $new_meta_value && $new_meta_value != $meta_value )
			update_post_meta( $post_id, $meta_key, $new_meta_value );
	}

}