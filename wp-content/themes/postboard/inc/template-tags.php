<?php
/**
 * Custom template tags for this theme.
 * Eventually, some of the functionality here could be replaced by core features.
 * 
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

if ( ! function_exists( 'postboard_site_branding' ) ) :
/**
 * Site branding for the site.
 * 
 * Display site title by default, but user can change it with their custom logo.
 * They can upload it on Customizer page.
 * 
 * @since  1.0.0
 */
function postboard_site_branding() {

	// Get the customizer value.
	$logo_id  = postboard_mod( PREFIX . 'logo' );

	// opening wrapper
	echo '<div id="logo" itemscope itemtype="http://schema.org/Brand">' . "\n";

	// Check if logo available, then display it.
	if ( $logo_id ) :
		echo '<a href="' . esc_url( get_home_url() ) . '" itemprop="url" rel="home">' . "\n";
			echo '<img itemprop="logo" src="' . esc_url( wp_get_attachment_url( $logo_id ) ) . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" />' . "\n";
		echo '</a>' . "\n";

	// If not, then display the Site Title
	else :
		echo '<h1 class="site-title" ' . hybrid_get_attr( 'site-title' ) . '><a href="' . esc_url( get_home_url() ) . '" itemprop="url" rel="home"><span itemprop="headline">' . esc_attr( get_bloginfo( 'name' ) ) . '</span></a></h1>'. "\n";
	endif;

	// display Site Description
		echo '<p class="site-description" ' . hybrid_get_attr( 'site-description' ) . '>' . esc_attr( get_bloginfo( 'description' ) ) . '</p>';

	// closing wrapper
	echo '</div>'. "\n";
}
endif;

if ( ! function_exists( 'postboard_social_icons' ) ) :
/**
 * Header social
 */
function postboard_social_icons( $position = 'header' ) {

	// Get the customizer data 
	$tw        = postboard_mod( PREFIX . 'twitter' );
	$fb        = postboard_mod( PREFIX . 'facebook' );
	$gplus     = postboard_mod( PREFIX . 'gplus' );
	$instagram = postboard_mod( PREFIX . 'instagram' );
	$pinterest = postboard_mod( PREFIX . 'pinterest' );
	$linkedin  = postboard_mod( PREFIX . 'linkedin' );
	$tumblr    = postboard_mod( PREFIX . 'tumblr' );
	$rss       = postboard_mod( PREFIX . 'rss' );

	// Display the data
	if ( $tw || $fb || $gplus || $instagram || $pinterest || $tumblr || $rss ) {
		echo '<div class="' . $position . '-social">';
			if ( $tw ) {
				echo '<a href="' . esc_url( $tw ) . '"><i class="fa fa-twitter"></i></a> ';
			}
			if ( $fb ) {
				echo '<a href="' . esc_url( $fb ) . '"><i class="fa fa-facebook"></i></a> ';
			}
			if ( $gplus ) {
				echo '<a href="' . esc_url( $gplus ) . '"><i class="fa fa-google-plus"></i></a> ';
			}
			if ( $instagram ) {
				echo '<a href="' . esc_url( $instagram ) . '"><i class="fa fa-instagram"></i></a> ';
			}
			if ( $pinterest ) {
				echo '<a href="' . esc_url( $pinterest ) . '"><i class="fa fa-pinterest"></i></a> ';
			}
			if ( $linkedin ) {
				echo '<a href="' . esc_url( $linkedin ) . '"><i class="fa fa-linkedin"></i></a> ';
			}
			if ( $tumblr ) {
				echo '<a href="' . esc_url( $tumblr ) . '"><i class="fa fa-tumblr"></i></a> ';
			}
			if ( $rss ) {
				echo '<a href="' . esc_url( $rss ) . '"><i class="fa fa-rss"></i></a>';
			}
		echo '</div>';
	}
	
}
endif;

if ( ! function_exists( 'postboard_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since 1.0.0
 */
function postboard_posted_on() {

	// Get the date style (from Single Posts)
	$date_style = postboard_mod( PREFIX . 'post-date-style' );
	?>
	<span class="entry-date">
		<i class="fa fa-clock-o"></i>
		<time class="published" datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>" <?php hybrid_attr( 'entry-published' ) ?>>
			<?php 
			if ( $date_style == 'absolute' ) {
				echo esc_html( get_the_date() );
			} else {
				printf( __( '%s ago', 'postboard' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) );
			}
			?>
		</time>
	</span>
	<?php
}
endif;

if ( ! function_exists( 'postboard_posted_on_single' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since 1.0.0
 */
function postboard_posted_on_single() {

	// Get customizer data
	$date = postboard_mod( PREFIX . 'post-date' );
	$date_style = postboard_mod( PREFIX . 'post-date-style' );

	if ( $date ) :
	?>
		<span class="date">
			<span><?php _e( 'Posted on', 'postboard' ); ?></span>
			<time class="published" datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>" <?php hybrid_attr( 'entry-published' ) ?>>
				<?php 
				if ( $date_style == 'absolute' ) {
					echo esc_html( get_the_date() );
				} else {
					printf( __( '%s ago', 'postboard' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) );
				}
				?>
			</time>
		</span>
	<?php
	endif;
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @since  1.0.0
 * @return bool
 */
function postboard_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'postboard_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'postboard_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so postboard_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so postboard_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in postboard_categorized_blog.
 *
 * @since 1.0.0
 */
function postboard_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'postboard_categories' );
}
add_action( 'edit_category', 'postboard_category_transient_flusher' );
add_action( 'save_post',     'postboard_category_transient_flusher' );

if ( ! function_exists( 'postboard_entry_share' ) ) :
/**
 * Social share.
 *
 * @since 1.0.0
 */
function postboard_entry_share() {

	// Get the data set in customizer
	$share  = postboard_mod( PREFIX . 'post-share' );

	if ( ! $share ) {
		return;
	}
	?>
		<span class="entry-share">
			<?php _e( 'Share:', 'postboard' ); ?>
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink( get_the_ID() ) ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
			<a href="https://twitter.com/intent/tweet?text=<?php echo esc_attr( get_the_title( get_the_ID() ) ); ?>&amp;url=<?php echo urlencode( get_permalink( get_the_ID() ) ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
			<a href="https://plus.google.com/share?url=<?php echo urlencode( get_permalink( get_the_ID() ) ); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
			<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode( get_permalink( get_the_ID() ) ); ?>&amp;title=<?php echo esc_attr( get_the_title( get_the_ID() ) ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
			<a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode( get_permalink( get_the_ID() ) ); ?>&amp;media=<?php echo urlencode( wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ) ); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
		</span>
	<?php
}
endif;

if ( ! function_exists( 'postboard_entry_author' ) ) :
/**
 * Post Author
 *
 * @since 1.0.0
 */
function postboard_entry_author() {

	// Get customizer data
	$author = postboard_mod( PREFIX . 'post-author' );

	if ( $author ) :
	?>
		<span class="author vcard" <?php hybrid_attr( 'entry-author' ); ?>>
			<?php 
			printf( 'by <a class="url fn n" href="%1$s" itemprop="url"><span itemprop="name">%2$s</span></a>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_html( get_the_author() )
			);
			?>
		</span>
	<?php
	endif;
}
endif;

if ( ! function_exists( 'postboard_post_author_box' ) ) :
/**
 * Author post informations.
 *
 * @since  1.0.0
 */
function postboard_post_author_box() {

	// Bail if not on the single post.
	if ( ! is_single() ) {
		return;
	}
?>
	<div class="entry-author" <?php hybrid_attr( 'entry-author' ) ?>>
		<h3 class="block-title"><?php _e( 'Written by', 'postboard' ); ?></h3>
		<article>
			<?php echo get_avatar( is_email( get_the_author_meta( 'user_email' ) ), apply_filters( 'postboard_author_bio_avatar_size', 96 ), '', strip_tags( get_the_author() ) ); ?>
			<div class="author-content">
				<h3 class="author-title name">
					<a class="author-name url fn n" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author" itemprop="url"><span itemprop="name"><?php echo strip_tags( get_the_author() ); ?></span></a>
				</h3>
				<p class="bio" itemprop="description"><?php echo stripslashes( get_the_author_meta( 'description' ) ); ?></p>
			</div>
		</article>
	</div><!-- .entry-author -->
<?php
}
endif;

if ( ! function_exists( 'postboard_related_posts' ) ) :
/**
 * Related posts.
 *
 * @since  1.0.0
 */
function postboard_related_posts() {

	// Get the data set in customizer
	$enable  = postboard_mod( PREFIX . 'related-posts' );

	// Disable if user choose it.
	if ( ! $enable ) {
		return;
	}
	?>
	<div class="entry-bottom wrap clearfix">

	<?php
		postboard_post_author_box(); // Display the author box.

		// Get the taxonomy terms of the current page for the specified taxonomy.
		$terms = wp_get_post_terms( get_the_ID(), 'category', array( 'fields' => 'ids' ) );

		// Bail if the term empty.
		if ( empty( $terms ) ) {
			return;
		}
		
		// Posts query arguments.
		$query = array(
			'post__not_in' => array( get_the_ID() ),
			'tax_query'    => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'id',
					'terms'    => $terms,
					'operator' => 'IN'
				)
			),
			'posts_per_page' => 1,
			'post_type'      => 'post',
		);

		// Allow dev to filter the query.
		$args = apply_filters( 'postboard_related_posts_args', $query );

		// The post query
		$related = new WP_Query( $args );

		if ( $related->have_posts() ) : ?>

			<div class="entry-related">
				<h3 class="block-title"><?php _e( 'Related Post', 'postboard' ); ?></h3>
				<article>
					<?php while ( $related->have_posts() ) : $related->the_post(); ?>
						<?php if ( has_post_thumbnail() ) : ?>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'entry-thumbnail', 'alt' => esc_attr( get_the_title() ) ) ); ?></a>
						<?php endif; ?>
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
						<div class="entry-summary"><?php the_excerpt(); ?></div>
					<?php endwhile; ?>
				</article>
			</div>
		
		<?php endif;

		// Restore original Post Data.
		wp_reset_postdata();
		?>

	</div><!-- .entry-bottom -->
	<?php
}
endif;

if ( ! function_exists( 'postboard_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since  1.0.0
 */
function postboard_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>" <?php hybrid_attr( 'comment' ); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="comment-container">
			<p <?php hybrid_attr( 'comment-content' ); ?>><?php _e( 'Pingback:', 'postboard' ); ?> <span <?php hybrid_attr( 'comment-author' ); ?>><span itemprop="name"><?php comment_author_link(); ?></span></span> <?php edit_comment_link( __( '(Edit)', 'postboard' ), '<span class="edit-link">', '</span>' ); ?></p>
		</article>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>" <?php hybrid_attr( 'comment' ); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="comment-wrapper">

			<div class="comment-avatar">
				<figure><?php echo get_avatar( $comment, apply_filters( 'postboard_comment_avatar_size', 64 ) ); ?></figure>
			</div>

			<div class="comment-detail">

				<div class="comment-name number">
					<span class="name" <?php hybrid_attr( 'comment-author' ); ?>><span itemprop="name"><?php echo get_comment_author_link(); ?></span></span>
					<?php echo postboard_comment_author_badge(); ?>

					<?php
						printf( '<span class="comment-date"><a href="%1$s" ' . hybrid_get_attr( 'comment-permalink' ) . '><time datetime="%2$s" ' . hybrid_get_attr( 'comment-published' ) . '>%3$s</time></a> %4$s</span>',
							esc_url( get_comment_link( $comment->comment_ID ) ),
							get_comment_time( 'c' ),
							/* translators: 1: date, 2: time */
							sprintf( __( '%1$s at %2$s', 'postboard' ), get_comment_date(), get_comment_time() ),
							sprintf( __( '%1$s&middot; Edit%2$s', 'postboard' ), '<a href="' . get_edit_comment_link() . '" title="' . esc_attr__( 'Edit Comment', 'postboard' ) . '">', '</a>' )
						);
					?>
				</div><!-- .comment-name -->
											
				<div class="comment-description" <?php hybrid_attr( 'comment-content' ); ?>>
					<?php if ( '0' == $comment->comment_approved ) : ?>
						<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'postboard' ); ?></p>
					<?php endif; ?>
					<?php comment_text(); ?>
					<span class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'postboard' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</span><!-- .reply -->
				</div><!-- .comment-description -->

			</div>

		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

if ( ! function_exists( 'postboard_comment_author_badge' ) ) :
/**
 * Custom badge for post author comment
 * 
 * @since  1.0.0
 */
function postboard_comment_author_badge() {

	// Set up empty variable
	$output = '';

	// Get comment classes
	$classes = get_comment_class();

	if ( in_array( 'bypostauthor', $classes ) ) {
		$output = '<span class="author-badge">' . __( 'Author', 'postboard' ) . '</span>';
	}

	// Display the badge
	return apply_filters( 'postboard_comment_author_badge', $output );
}
endif;

if ( ! function_exists( 'postboard_comment_count' ) ) :
/**
 * Comment count
 *
 * @since  1.0.0
 */
function postboard_comment_count() {

	// Get the default value.
	$enable = postboard_mod( PREFIX . 'post-comment' );

	if ( $enable ) :
		if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
			<span class="entry-comments">
				<?php 
				_e( 'Comments', 'postboard' ); 
				comments_popup_link( '0', '1', '%' ); 
				?>
			</span>
	<?php endif;
	endif; 

}
endif;

if ( ! function_exists( 'postboard_get_first_image' ) ) :
/**
 * Return an HTML img tag for the first image in a post content. Used to draw
 * the content for posts of the �image� format.
 * http://css-tricks.com/snippets/wordpress/get-the-first-image-from-a-post/#comment-1582091 --> not working
 * http://www.wprecipes.com/how-to-get-the-first-image-from-the-post-and-display-it
 *
 * @return string An HTML img tag for the first image in a post content.
 */
function postboard_get_first_image( $size = 'full', $echo = true ) {

	// TO-DO: handle when $echo is false
	if ( has_post_thumbnail() && $echo ) {
		return postboard_featured_image( $size );
	}

	// Expose information about the current post.
	global $post;

	// We'll trap to see if this stays empty later in the function.
	$src = '';

	// Grab all img src's in the post content
	// $output = preg_match_all( '//i', $post->post_content, $matches ); // not working
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

	// Grab the first img src returned by our regex.
	// if ( ! isset ( $matches[1][0] ) ) { return false; }
	// $src = $matches[1][0];

	// Grab the first <img> complete markup returned by our regex.
	if ( ! isset ( $matches[0][0] ) ) { return false; }
	$src = $matches[0][0];

	// Make sure there's still something worth outputting after sanitization.
	if ( empty( $src ) ) { return false; }

	// add wrapper
	$content = '<div class="entry-image clearfix">';
		$content .= '<a class="img-link" href="' . get_the_permalink() . '">';
			$content .= $src;
		$content .= '</a>';
	$content .= '</div>';

	// choose whether to echo the result or return it as variable
	if ( true == $echo )
		echo $content;
	else
		return $src;

}
endif;

if ( ! function_exists( 'postboard_get_format_gallery' ) ) :
/**
 * Get the [gallery] shortcode from the post content and display it on index page. It require
 * gallery ids [gallery ids=1,2,3,4] to display it as thumbnail slideshow. If no ids exist it
 * just display it as postboard [gallery] markup.
 *
 * If no [gallery] shortcode found in the post content, get the attached images to the post and 
 * display it as slideshow.
 * 
 * @since  1.0.0
 * @uses   get_post_gallery() to get the gallery in the post content.
 * @uses   wp_get_attachment_image() to get the HTML image.
 * @uses   get_children() to get the attached images if no [gallery] found in the post content.
 * @return string
 */
function postboard_get_format_gallery() {

	// Set up placeholders
	$slider = '';
	$carousel = '';

	$size = 'large';

	// Check the [gallery] shortcode in post content.
	$gallery = get_post_gallery( get_the_ID(), false );

	// Check if the [gallery] exist.
	if ( $gallery ) {

		// Check if the gallery ids exist.
		if ( isset( $gallery['ids'] ) ) {

			// Get the gallery ids and convert it into array.
			$ids = explode( ',', $gallery['ids'] );

			// Display the gallery in a cool slideshow on index page.
			foreach( $ids as $id ) {
				$slider .= '<li>';
				$slider .= wp_get_attachment_image( $id, $size, false, array( 'class' => 'entry-thumbnail' ) );
				$slider .= '</li>';
			}

		} else {

			// If gallery ids not exist, display the postboard gallery markup.
			// avoid this, since it'll look bad
			// $html = get_post_gallery( get_the_ID() );

			// if gallery based on images attached to post (only [gallery] in post content)
			// note: in the post content, better to use: [gallery size="large"] or [gallery size="full"]
			$srcs = $gallery['src'];

			// Display the gallery in a cool slideshow on index page.
			foreach( $srcs as $src ) {
				$slider .= '<li >';
				$slider .= '<img src="' . esc_url( $src ) . '" />';
				$slider .= '</li>';
			}

		}

	// If no [gallery] in post content, get attached images to the post.
	} else {

		// Set up default arguments.
		$defaults = array( 
			'order'          => 'ASC',
			'post_type'      => 'attachment',
			'post_parent'    => get_the_ID(),
			'post_mime_type' => 'image',
			'numberposts'    => -1
		);

		// Retrieves attachments from the post.
		$attachments = get_children( apply_filters( 'postboard_gallery_format_args', $defaults ) );

		// Check if attachments exist.
		if ( $attachments ) {

			// Display the attachment images in a cool slideshow on index page.
			foreach ( $attachments as $attachment ) {
				$slider .= '<li>';
				$slider .= wp_get_attachment_image( $attachment->ID, $size, false, array( 'class' => 'entry-thumbnail' ) );
				$slider .= '</li>';
			}

		} else {

			// if no [gallery] shortcode and has no attachments, bail them out
			return;

		}

	}
	?>
	<div class="entry-image bxslider-wrap">
		<ul class="bxslider">
			<?php echo $slider; ?>
		</ul>
	</div><!-- .entry-image -->
	<?php
}
endif;

if ( ! function_exists( 'postboard_get_post_format_link_url' ) ) :
/**
 * Forked from hybrid_get_the_post_format_url.
 * Filters 'get_the_post_format_url' to make for a more robust and back-compatible function.  If WP did 
 * not find a URL, check the post content for one.  If nothing is found, return the post permalink.
 * Used in Post Format Link
 *
 * @since 1.0.0
 */
function postboard_get_post_format_link_url( $url = '', $post = null ) {

	if ( empty( $url ) ) {

		$post = is_null( $post ) ? get_post() : $post;

		/* Catch links that are not wrapped in an '<a>' tag. */
		$content_url = preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', make_clickable( $post->post_content ), $matches );

		$content_url = ! empty( $matches[1] ) ? esc_url_raw( $matches[1] ) : '';

		$url = ! empty( $content_url ) ? $content_url : get_permalink( get_the_ID() );
	}

	if ( $url ) {
	?>
		<h2 class="entry-title">
			<a href="<?php echo esc_url( $url ); ?>" itemprop="url">
				<?php if ( get_the_title() && ( __( '(Untitled)', 'postboard' ) != get_the_title() ) ) { ?>
					<?php the_title(); ?>
				<?php } else { ?>
					<?php echo esc_attr( $url ); ?>
				<?php }	?>
			</a>
		</h2>
	<?php
	}
	
}
endif;

if ( ! function_exists( 'postboard_footer_text' ) ) :
/**
 * Footer Text
 *
 * @since  1.0.0
 */
function postboard_footer_text() {

	// Get the customizer data 
	$footer_text = postboard_mod( PREFIX . 'footer-text' );

	// Polylang integration
	if ( is_polylang_activated() ) {
		$footer_text = pll__( postboard_mod( PREFIX . 'footer-text' ) );
	}

	// Display the data
	echo '<div class="copyright">&copy; 2018  HeroG-tech</div>';
	
}
endif;

if ( ! function_exists( 'postboard_header_style' ) ) :
/**
 * Header class for header style
 */
function postboard_header_style() {
	// Get the customizer data
	$header_style = postboard_mod( PREFIX . 'header-style' );

	// Print the header class
	if ( $header_style )
		echo esc_attr( $header_style );
}
endif;

if ( ! function_exists( 'postboard_posts_layout' ) ) :
/**
 * Custom archive page layout classes.
 *
 * @since  1.0.0
 */
function postboard_posts_layout() {

	// Get the default layout.
	if ( is_category() ) {
		$layout = postboard_mod( PREFIX . 'cat-layout' );
	} elseif ( is_tag() ) {
		$layout = postboard_mod( PREFIX . 'tag-layout' );
	} elseif ( is_author() ) {
		$layout = postboard_mod( PREFIX . 'author-layout' );
	} elseif ( is_search() ) {
		$layout = postboard_mod( PREFIX . 'search-layout' );
	} else {
		$layout = postboard_mod( PREFIX . 'archive-layout' );
	}

	// Set up empty variable.
	$class = '';

	if ( 'blog' === $layout ) {
		$class = 'loop-blog';
	} elseif ( 'list' === $layout ) {
		$class = 'loop-list';
	} elseif ( 'grid' === $layout ) {
		$class = 'loop-grid';
	} 

	// Allow dev to filter the class.
	echo esc_attr( apply_filters( 'postboard_archive_page_classes', $class ) );

}
endif;

if ( ! function_exists( 'postboard_featured_image' ) ) :
/**
 * Featured Image
 * 
 * @since  1.0.0
 */
function postboard_featured_image( $size='large' ) {

	// always true for Archive
	$enable = true;

	// Get the data set in customizer
	if ( is_single() ) {
		$enable = postboard_mod( PREFIX . 'post-thumbnail' );
	}

	if ( $enable && has_post_thumbnail() ) :
	?>
		<div class="entry-image clearfix">
			<?php if ( ! is_single() ) : ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php endif; ?>
				<?php the_post_thumbnail( $size, array( 'class' => 'entry-thumbnail', 'alt' => esc_attr( get_the_title() ) ) ); ?>
			<?php if ( ! is_single() ) : ?>
				</a>
			<?php endif; ?>
		</div>
	<?php
	endif;
}
endif;


if ( ! function_exists( 'postboard_entry_category' ) ) :
/**
 * Prints HTML with Category for the current post.
 *
 * @since 1.0.0
 */
function postboard_entry_category( $icon = false ) {

	// bail out if not 'Post'
	if ( 'post' != get_post_type() ) 
		return;

	// always true for Archive
	$enable = true;

	// Get the data set in customizer
	if ( is_single() ) {
		$enable = postboard_mod( PREFIX . 'post-cat' );
	} 

	$category = get_the_category( get_the_ID() );
	if ( $enable && get_the_category( get_the_ID() ) ) :
	?>
		<span class="entry-category" <?php hybrid_attr( 'entry-terms', 'category' ); ?>>
			<?php if ( $icon ) : ?><i class="fa fa-folder"></i><?php endif; ?>
			<a href="<?php echo esc_url( get_category_link( $category[0]->term_id ) ); ?>"><?php echo esc_attr( $category[0]->name ); ?></a>
		</span><!-- .entry-category -->
	<?php endif; // End if category 

}
endif;

if ( ! function_exists( 'postboard_entry_tags' ) ) :
/**
 * Prints HTML with Tags for the current post.
 *
 * @since 1.0.0
 */
function postboard_entry_tags() {

	// Hide tags if not in Single Post
	if ( ! is_singular( 'post' ) ) 
		return;

	// Get the data set in customizer
	$tag = postboard_mod( PREFIX . 'post-tag' );

	if ( $tag ) :
		if ( get_the_tag_list() ) :
		?>
			<span class="entry-tags" <?php hybrid_attr( 'entry-terms', 'post_tag' ); ?>>
				<?php echo get_the_tag_list( '', ' ' ); ?>
			</span>
		<?php endif; // End if $tags_list ?>
	<?php endif;
}
endif;

if ( ! function_exists( 'postboard_entry_like' ) ) :
/**
 * Show Like Counter HTML
 */
function postboard_entry_like() {

	// Always true for Archive
	$enable = true;

	if ( is_single() ) {
		$enable = postboard_mod( PREFIX . 'post-like' );
	}

	if ( $enable ) :
		echo postboard_do_likes();
	endif;
}
endif;

if ( ! function_exists( 'postboard_featured_slider' ) ) :
/**
 * Content Slider
 */
function postboard_featured_slider() {

	// Display on home page only
	if ( ! is_home() ) {
		return;
	}

	// Get the data set in customizer
	$enable  = postboard_mod( PREFIX . 'featured-enable' );
	$tag     = postboard_mod( PREFIX . 'featured-tag' );
	$num     = postboard_mod( PREFIX . 'featured-num' );
	$orderby = postboard_mod( PREFIX . 'featured-orderby' );

	// Check if it enabled.
	if ( ! $enable )
		return;

	// Posts query arguments.
	$args = array(
		'post_type'           => 'post',
		'posts_per_page'      => absint( $num ),
		'orderby'             => $orderby,
		'ignore_sticky_posts' => 1
	);

	// Limit to tag based on user selected tag.
	if ( ! empty( $tag ) ) {
		$args['tag_id'] = $tag;
	}

	// Allow dev to filter the post arguments.
	$args = apply_filters( 'postboard_featured_slider_args', $args );

	// The post query.
	$slider = new WP_Query( $args );

	if ( $slider->have_posts() ) :
?>
		<div id="featured-content" class="bxslider-wrap container">
			<div class="entry-image">
				<ul class="bxslider">

					<?php while ( $slider->have_posts() ) : $slider->the_post(); ?>
						
						<li>
							<a href="<?php the_permalink(); ?>">
								<?php if ( has_post_thumbnail() ) : ?>
									<?php the_post_thumbnail( 'large', array( 'alt' => esc_attr( get_the_title() ) ) ); ?>
								<?php else : ?>
									<img class="entry-thumbnail" width="1080" height="620" src="<?php echo esc_url( '//placehold.it/1080x620/?text=' . urlencode( get_the_title() ) ); ?>" alt="<?php the_title_attribute(); ?>" />
								<?php endif; ?>
							</a>
							<div class="featured-overlay">
								<div class="entry-header">
									<div class="entry-meta">
										<?php postboard_entry_category( true ); ?>
										<?php postboard_posted_on(); ?>
										<?php postboard_entry_like(); ?>
									</div>							
									<?php the_title( sprintf( '<h2 class="entry-title" ' . hybrid_get_attr( 'entry-title' ) . '><a href="%s" itemprop="url" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
								</div>
							</div>
						</li>

					<?php endwhile; ?>

				</ul><!-- .slides -->
			</div><!-- .entry-image -->
		</div><!-- #featured-content -->
	<?php
	endif;

	wp_reset_postdata();
}
endif;

if ( ! function_exists( 'postboard_loop_title' ) ) :
/**
 * Footer Text
 *
 * @since  1.0.0
 */
function postboard_loop_title() {

	// Get the customizer data 
	$heading     = postboard_mod( PREFIX . 'loop-title' );
	$description = postboard_mod( PREFIX . 'loop-description' );

	// Polylang integration
	if ( is_polylang_activated() ) {
		$heading = pll__( postboard_mod( PREFIX . 'loop-title' ) );
		$description = pll__( postboard_mod( PREFIX . 'loop-description' ) );
	}
	// Display the data
	?>
	<h3 class="section-heading"><?php echo stripslashes( $heading ); ?></h3>
	<p class="section-description"><?php echo stripslashes( $description ); ?></p>
	<?php
}
endif;

if ( ! function_exists( 'postboard_post_sorter' ) ) :
/**
 * Post Sorter in archive pages
 * 'post_date'     = Latest
 * 'like'          = Likes
 * 'comment_count' = Comments
 * 'rand'          = Random
 */
function postboard_post_sorter() {

	// get variable from URL
	$orderby = get_query_var( 'orderby' );

	if ( $orderby ) {
		// this is for 'Likes' filter, since it's customized one
		if ( ( 'post_date' != $orderby ) && ( 'comment_count' != $orderby ) && ( 'rand' != $orderby ) ) {
			$sorter = 'like';
		} else {
			// other filters, just grab them right away
			$sorter = $orderby;
		}
	} else {
		// default throwback to 'Latest'
		$sorter = 'post_date';
	}
	?>
	<span class="post-sorter">
		<strong><?php esc_html_e( 'Sort by:', 'postboard' ); ?></strong>

		<a href="<?php echo esc_url( add_query_arg( 'orderby', 'post_date' ) ); ?>" <?php echo ( ( 'post_date' == $sorter ) ? ' class="' . esc_attr( 'current' ) . '"' : '' ); ?>><?php esc_html_e( 'Latest', 'postboard' ); ?></a> / 

		<a href="<?php echo esc_url( add_query_arg( 'orderby', 'like' ) ); ?>" <?php echo ( ( 'like' == $sorter ) ? ' class="' . esc_attr( 'current' ) . '"' : '' ); ?>><?php esc_html_e( 'Likes', 'postboard' ); ?></a> / 

		<a href="<?php echo esc_url( add_query_arg( 'orderby', 'comment_count' ) ); ?>" <?php echo ( ( 'comment_count' == $sorter ) ? ' class="' . esc_attr( 'current' ) . '"' : '' ); ?>><?php esc_html_e( 'Comments', 'postboard' ); ?></a> / 

		<a href="<?php echo esc_url( add_query_arg( 'orderby', 'rand' ) ); ?>" <?php echo ( ( 'rand' == $sorter ) ? ' class="' . esc_attr( 'current' ) . '"' : '' ); ?>><?php esc_html_e( 'Random', 'postboard' ); ?></a>
	</span>
	<?php
}
endif;