<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * @since  1.0.0
 * @param  array $args Configuration arguments.
 * @return array
 */
function postboard_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'postboard_page_menu_args' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since  1.0.0
 * @param  array $classes Classes for the body element.
 * @return array
 */
function postboard_body_classes( $classes ) {

	// Adds a class of multi-author to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'multi-author';
	}

	if ( is_home() || is_archive() || is_search() ) {
		if ( postboard_mod( PREFIX . 'archive-layout' ) )
			$classes[] = postboard_mod( PREFIX . 'archive-layout' );
	}

	$menu_class = postboard_mod( PREFIX . 'menu-style' );
	if ( $menu_class ) {
		$classes[] = $menu_class;
	}

	if ( is_singular() ) {
		$classes[] = 'single';
	}

	return $classes;
}
add_filter( 'body_class', 'postboard_body_classes' );

/**
 * Adds custom classes to the array of post classes.
 *
 * @since  1.0.0
 * @param  array $classes Classes for the post element.
 * @return array
 */
function postboard_post_classes( $classes ) {

	// Adds a class if a post hasn't a thumbnail.
	if ( ! has_post_thumbnail() ) {
		$classes[] = 'no-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'postboard_post_classes' );

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 * Filters wp_title to print a neat <title> tag based on what is being viewed.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 * @return string The filtered title.
	 */
	function postboard_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}

		global $page, $paged;

		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}

		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( __( 'Page %s', 'postboard' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'postboard_wp_title', 10, 2 );

	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function postboard_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'postboard_render_title' );
endif;

/**
 * Change the excerpt more string.
 *
 * @since  1.0.0
 * @param  string  $more
 * @return string
 */
function postboard_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter( 'excerpt_more', 'postboard_excerpt_more' );

/**
 * Change excerpt character length
 */
function postboard_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'postboard_excerpt_length', 999 );

/**
 * Remove theme-layouts meta box on attachment and bbPress post type.
 * 
 * @since 1.0.0
 */
function postboard_remove_theme_layout_metabox() {
	remove_post_type_support( 'attachment', 'theme-layouts' );

	// bbPress
	remove_post_type_support( 'forum', 'theme-layouts' );
	remove_post_type_support( 'topic', 'theme-layouts' );
	remove_post_type_support( 'reply', 'theme-layouts' );
}
add_action( 'init', 'postboard_remove_theme_layout_metabox', 11 );

/**
 * Add post type 'post' support for the Simple Page Sidebars plugin.
 * 
 * @since  1.0.0
 */
function postboard_page_sidebar_plugin() {
	if ( class_exists( 'Simple_Page_Sidebars' ) ) {
		add_post_type_support( 'post', 'simple-page-sidebars' );
	}
}
add_action( 'init', 'postboard_page_sidebar_plugin' );

/**
 * Extend archive title
 *
 * @since  1.0.0
 */
function postboard_extend_archive_title( $title ) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = get_the_author();
	}
	return $title;
}
add_filter( 'get_the_archive_title', 'postboard_extend_archive_title' );

/**
 * Remove <p> from archive description
 * https://wordpress.org/support/topic/why-does-the_archive_description-output-p-tags
 */
function postboard_custom_archive_description($description) {
	$remove = array( '<p>', '</p>' );

	$description = str_replace( $remove, "", $description );

	return $description;
}
add_filter( 'get_the_archive_description', 'postboard_custom_archive_description' );

/**
 * Customize tag cloud widget
 *
 * @since  1.0.0
 */
function postboard_customize_tag_cloud( $args ) {
	$args['largest']  = 12;
	$args['smallest'] = 12;
	$args['unit']     = 'px';
	$args['number']   = 20;
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'postboard_customize_tag_cloud' );

/**
 * Modifies the theme layout on attachment pages.
 *
 * @since  1.0.0
 */
function postboard_mod_theme_layout( $layout ) {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$post_layout = get_post_layout( get_queried_object_id() );
		if ( 'default' === $post_layout ) {
			$layout = '1c';
		}
	}

	if ( is_home() || is_archive() || is_search() ) {	
		$layout = '1c';
	}

	return $layout;
}
add_filter( 'theme_mod_theme_layout', 'postboard_mod_theme_layout', 15 );

/**
 * Custom comment form fields.
 *
 * @since  1.0.0
 * @param  array $fields
 * @return array
 */
function postboard_comment_form_fields( $fields, $args = array() ) {

	$args = wp_parse_args( $args );
	if ( ! isset( $args['format'] ) )
		$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

	$commenter = wp_get_current_commenter();
	$req       = get_option( 'require_name_email' );
	$aria_req  = ( $req ? " aria-required='true'" : '' );
	$html5     = 'html5' === $args['format'];

	$fields['author'] = '<p class="comment-form-author"><input class="txt" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" placeholder="' . __( 'Name', 'postboard' ) . ( $req ? ' *' : '' ) . '"' . $aria_req . ' /></p>';

	$fields['email'] = '<p class="comment-form-email"><input class="txt" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" placeholder="' . esc_attr__( 'Email', 'postboard' ) . ( $req ? ' *' : '' ) . '"' . $aria_req . ' /></p>';

	$fields['url'] = '<p class="comment-form-url"><input class="txt" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" placeholder="' . esc_attr__( 'Website', 'postboard' ) . '" /></p>';

	return $fields;

}
add_filter( 'comment_form_default_fields', 'postboard_comment_form_fields' );

if ( ! function_exists( 'postboard_post_filter' ) ) :
/**
 * Post Filter
 * mostly to get 'Likes' orderby var
 */
function postboard_post_filter( $query ) {
	// get var from URL
	if ( get_query_var( 'orderby' ) && 'like' == get_query_var( 'orderby' ) ) {
		// make sure it doesn't run on admin
		if ( ! is_admin() && $query->is_main_query() ) {
			// set query args
			$query->set( 'meta_key', '_tj_likes' );
			$query->set( 'orderby', array( 'meta_value_num' => 'DESC' ) );
		}
	}
}
add_action( 'pre_get_posts', 'postboard_post_filter' );
endif;

if ( ! function_exists( 'postboard_strip_shortcode_gallery' ) ) :
/**
 * Remove [gallery] from Single Post the_content
 * http://wordpress.stackexchange.com/questions/121489/split-content-and-gallery/121508#121508
 */
function postboard_strip_shortcode_gallery( $content ) {
	preg_match_all( '/'. get_shortcode_regex() .'/s', $content, $matches, PREG_SET_ORDER );
	if ( ! empty( $matches ) ) {
		foreach ( $matches as $shortcode ) {
			if ( 'gallery' === $shortcode[2] ) {
				$pos = strpos( $content, $shortcode[0] );
				if ($pos !== false)
					return substr_replace( $content, '', $pos, strlen($shortcode[0]) );
			}
		}
	}
	return $content;
}
add_filter( 'the_content', 'postboard_strip_shortcode_gallery' );
endif;