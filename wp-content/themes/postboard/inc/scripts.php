<?php
/**
 * Enqueue scripts and styles.
 *
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

/**
 * Loads the theme styles & scripts.
 *
 * @since 1.0.0
 * @link  http://codex.wordpress.org/Function_Reference/wp_enqueue_script
 * @link  http://codex.wordpress.org/Function_Reference/wp_enqueue_style
 */
function postboard_enqueue() {

	// Load plugins stylesheet
	wp_enqueue_style( 'postboard-plugins-style', trailingslashit( get_template_directory_uri() ) . 'assets/css/plugins.min.css' );

	// Load Masonry
	wp_enqueue_script( 'masonry' );

	// if WP_DEBUG and/or SCRIPT_DEBUG turned on, load the unminified styles & script.
	if ( ! is_child_theme() && WP_DEBUG || SCRIPT_DEBUG ) {

		// Load main stylesheet
		wp_enqueue_style( 'postboard-style', get_stylesheet_uri() );

		// Load custom js plugins.
		wp_enqueue_script( 'postboard-plugins', trailingslashit( get_template_directory_uri() ) . 'assets/js/plugins.min.js', array( 'jquery' ), null, true );

		// Load custom js methods.
		wp_enqueue_script( 'postboard-main', trailingslashit( get_template_directory_uri() ) . 'assets/js/main.js', array( 'jquery' ), null, true );
		$script_handle = 'postboard-main';

	} else {

		// Load main stylesheet
		wp_enqueue_style( 'postboard-style', trailingslashit( get_template_directory_uri() ) . 'style.min.css' );

		// Load custom js plugins.
		wp_enqueue_script( 'postboard-scripts', trailingslashit( get_template_directory_uri() ) . 'assets/js/postboard.min.js', array( 'jquery' ), null, true );
		$script_handle = 'postboard-scripts';

	}

	// pass var to js
	wp_localize_script( $script_handle, 'postboard',
		array(
			'endofpages' => esc_html__( 'No more pages to load.', 'postboard' ),
			'site_url'   => trailingslashit( get_template_directory_uri() ),
			'ajaxurl'    => admin_url('admin-ajax.php'),
			'rated'      => esc_html__( 'You already like this', 'postboard' )
		)
	);

	// If child theme is active, load the stylesheet.
	if ( is_child_theme() ) {
		wp_enqueue_style( 'postboard-child-style', get_stylesheet_uri() );
	}

	// Load comment-reply script.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Loads HTML5 Shiv
	wp_enqueue_script( 'postboard-html5', trailingslashit( get_template_directory_uri() ) . 'assets/js/html5shiv.min.js', array( 'jquery' ), null, false );
	wp_script_add_data( 'postboard-html5', 'conditional', 'lte IE 9' );

}
add_action( 'wp_enqueue_scripts', 'postboard_enqueue' );