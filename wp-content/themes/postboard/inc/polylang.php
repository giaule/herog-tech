<?php
/**
 * Polylang Compatibility File
 *
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

/**
 * Register footer text strings
 */
$footer_text = postboard_mod( PREFIX . 'footer-text' ); // Get the data set in customizer
pll_register_string( PREFIX . 'footer-text', $footer_text, 'PostBoard' ); // Register string

/**
 * Register loop title strings
 */
 // Get the data set in customizer
$heading = pll__( postboard_mod( PREFIX . 'loop-title' ) );
$description = pll__( postboard_mod( PREFIX . 'loop-description' ) );

// Register string
pll_register_string( PREFIX . 'loop-title', $heading, 'PostBoard' ); 
pll_register_string( PREFIX . 'loop-description', $description, 'PostBoard' ); 