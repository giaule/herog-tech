<?php
/**
 * Header color
 *
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

if ( ! function_exists( 'postboard_customizer_header_styles' ) && class_exists( 'Customizer_Library_Styles' ) ) :
/**
 * Process user options to generate CSS needed to implement the choices.
 *
 * @since  1.0.0
 */
function postboard_customizer_header_styles() {


	// Header Background Color
	$header = postboard_mod( PREFIX . 'header-color' );

	if ( $header !== customizer_library_get_default( PREFIX . 'header-color' ) ) {

		$color = sanitize_hex_color( $header );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'#masthead'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}

	// Site title color
	$title = postboard_mod( PREFIX . 'site-title-color' );

	if ( $title !== customizer_library_get_default( PREFIX . 'site-title-color' ) ) {

		$color = sanitize_hex_color( $title );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'#masthead .site-title a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}

}
endif;
add_action( 'postboard_customizer_library_styles', 'postboard_customizer_header_styles' );