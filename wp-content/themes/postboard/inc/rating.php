<?php
/**
 * Modified from ZillaLikes
 * http://www.themezilla.com/plugins/zillalikes/
 * 
 * @package    PostBoard
 * @author     Theme Junkie
 * @copyright  Copyright (c) 2015, Theme Junkie
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

/**
 * add post meta '_tj_likes'
 */
function postboard_setup_likes( $post_id ) {
	if ( ! is_numeric( $post_id ) ) return;
	
	add_post_meta( $post_id, '_tj_likes', '0', true );
}
add_action( 'publish_post', 'postboard_setup_likes' );

/**
 * AJAX callback, to get the refreshed like count
 */
function postboard_ajax_callback( $post_id ) {

	if( isset( $_POST['likes_id'] ) ) {
		// Click event. Get and Update Count
		$post_id = str_replace( 'tj-likes-', '', $_POST['likes_id'] );
		echo postboard_like_this( $post_id, 'update' );
	} else {
		// AJAXing data in. Get Count
		$post_id = str_replace( 'tj-likes-', '', $_POST['post_id'] );
		echo postboard_like_this( $post_id, 'get' );
	}

	exit;
}
add_action('wp_ajax_postboard-likes', 'postboard_ajax_callback' );
add_action('wp_ajax_nopriv_postboard-likes', 'postboard_ajax_callback' );

/**
 * get the like count
 */
function postboard_like_this( $post_id, $action = 'get' ) {

	if( ! is_numeric( $post_id ) ) return;

	switch( $action ) {
		
		case 'get':
			$likes = get_post_meta( $post_id, '_tj_likes', true );
			if( ! $likes ){
				$likes = 0;
				add_post_meta( $post_id, '_tj_likes', $likes, true );
			}

			return '<i class="fa fa-heart"></i> <span class="tj-likes-count">'. esc_html( $likes ) .'</span>';
		break;

		case 'update':
			$likes = get_post_meta( $post_id, '_tj_likes', true );
			if( isset( $_COOKIE['tj_likes_'. $post_id] ) ) return $likes;

			$likes++;
			update_post_meta( $post_id, '_tj_likes', $likes );
			setcookie( 'tj_likes_'. $post_id, $post_id, time()*20, '/' );

			return '<i class="fa fa-heart"></i> <span class="tj-likes-count">'. esc_html( $likes ) .'</span>';
		break;
		
	}
}

/**
 * Template Tag for Like counter
 */
function postboard_do_likes() {

	// get the like count
	$output = postboard_like_this( get_the_ID() );

	$class = 'entry-like';
	$active_class = 'none';
	$title = __( 'Like this', 'postboard' );

	// check cookie, if user already like
	if( isset( $_COOKIE['tj_likes_'. get_the_ID()] ) ) {
		$active_class = 'active';
		$title = __( 'You already like this', 'postboard' );
	}

	return '<span class="' . esc_attr( $class ) .'"><a href="#" class="' . esc_attr( $active_class ) . '" id="tj-likes-'. get_the_ID() .'" title="'. esc_attr( $title ) . '">' . $output . '</a></span>';
}