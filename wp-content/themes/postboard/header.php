<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php hybrid_attr( 'body' ); ?>>

<div id="page" class="hfeed site">

	<!-- Primary Bar / Start -->
	<div id="primary-bar" class="clearfix">
		<div class="container inner">

			<?php get_template_part( 'menu', 'primary' ); // Loads the menu-primary.php template. ?>

			<?php postboard_social_icons(); ?>

		</div><!-- .container -->
	</div>	
	<!-- Primary Bar / End -->

	<header id="masthead" class="site-header <?php postboard_header_style(); ?> container clearfix" <?php hybrid_attr( 'header' ); ?>>

		<?php postboard_site_branding(); ?>

		<?php postboard_header_ads(); // Get the header advertisement data. ?>

	</header><!-- #masthead -->

	<div id="secondary-bar" class="container">
		<?php get_template_part( 'menu', 'secondary' ); ?>		
	</div>

	<?php postboard_featured_slider(); // Get the featured slider. ?>
	
	<!-- Site Main / Start -->
	<main id="main" class="site-main container clearfix" <?php hybrid_attr( 'content' ); ?>>