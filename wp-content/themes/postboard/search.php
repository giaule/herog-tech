<?php get_header(); ?>

	<div id="primary" class="content-area">

		<?php if ( have_posts() ) : ?>

			<div class="intro">
				'<h3 class="section-heading"><?php printf( __( 'Search Results for: %s', 'postboard' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
			</div><!-- .intro -->

			<div class="content-loop grid clearfix">

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content' ); ?>

				<?php endwhile; ?>

			</div><!-- .content-loop -->

			<?php get_template_part( 'loop', 'nav' ); // Loads the loop-nav.php template ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

	</section><!-- #primary -->

	<?php get_sidebar(); ?>
<?php get_footer(); ?>
