<section class="no-results not-found single">

	<article class="hentry">

		<header class="entry-header wrap">
			<h1 class="page-title"><?php _e( 'Nothing Found', 'postboard' ); ?></h1>
		</header><!-- .page-header -->

		<div class="entry-content wrap clearfix">
			<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

				<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'postboard' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

			<?php elseif ( is_search() ) : ?>

				<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'postboard' ); ?></p>
				<p><?php get_search_form(); ?></p>

			<?php else : ?>

				<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'postboard' ); ?></p>
				<?php get_search_form(); ?>

			<?php endif; ?>
		</div><!-- .entry-content -->

	</article>

</section><!-- .no-results -->