<?php
// Check if there's a menu assigned to the 'primary' location.
if ( ! has_nav_menu( 'secondary' ) ) {
	return;
}
?>
<!-- Secondary Navigation / Start -->
<nav id="secondary-nav" class="container inner clearfix" <?php hybrid_attr( 'menu' ); ?>>

	<?php wp_nav_menu(
		array(
			'theme_location'  => 'secondary',
			'container'       => false,
			'menu_id'         => 'secondary-menu',
			'menu_class'      => 'sf-menu',
			'fallback_cb'     => '',
			'walker'          => new PostBoard_Custom_Nav_Walker
		)
	); ?>

	<?php
	$search_header = postboard_mod( PREFIX . 'search-header' ); 
	if ( $search_header ) :
	?>
	<div class="header-search">
		<form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<button type="submit" id="search-submit" class="fa fa-search"></button>
			<input type="search" name="s" id="s" placeholder="<?php esc_attr_e( 'Search for &hellip;', 'postboard' ); ?>" autocomplete="off" value="<?php echo esc_attr( get_search_query() ); ?>" />
		</form>
	</div>
	<?php endif; ?>

</nav><!-- #secondary-nav -->
<!-- Secondary Navigation / End -->