<?php get_header(); ?>

	<div id="primary" class="content-area">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php get_template_part( 'loop', 'nav' ); // Loads the loop-nav.php template  ?>

			<?php postboard_related_posts(); // Display the related posts. ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

	</div><!-- #primary -->

	<?php get_sidebar(); ?>
<?php get_footer(); ?>