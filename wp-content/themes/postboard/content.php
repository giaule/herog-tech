<?php

$grid_style = postboard_mod( PREFIX . 'archive-width' );

$class = 'grid-item';

if ( ( 'default' != $grid_style ) && ( get_post_meta( get_the_ID(), 'postboard_post_width', true ) ) )
	$class = 'grid-item ' . get_post_meta( get_the_ID(), 'postboard_post_width', true );
?>
<div class="<?php echo esc_attr( $class ); ?>">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php hybrid_attr( 'post' ); ?>>

		<?php if ( has_post_format( 'video' ) ) : ?>
			<div class="entry-image video">
				<?php echo hybrid_media_grabber( array( 'type' => 'video', 'split_media' => true ) ); ?>
			</div>
		<?php elseif ( has_post_format( 'audio' ) ) : ?>
			<div class="entry-image audio">
				<?php echo hybrid_media_grabber( array( 'type' => 'audio', 'split_media' => true ) ); ?>
			</div>
		<?php elseif ( has_post_format( 'image' ) )	: ?>
			<?php postboard_get_first_image( 'large' ); ?>
		<?php elseif ( has_post_format( 'gallery' ) ) : ?>
			<a href="<?php the_permalink(); ?>"><?php echo postboard_get_format_gallery(); // Get the gallery ?></a>
		<?php else : ?>
			<?php postboard_featured_image(); ?>
		<?php endif; ?>

		<header class="entry-header">
			<?php 
			if ( has_post_format( 'link' ) ) {
				postboard_get_post_format_link_url();
			} else {
				the_title( sprintf( '<h2 class="entry-title" ' . hybrid_get_attr( 'entry-title' ) . '><a href="%s" rel="bookmark" itemprop="url">', esc_url( get_permalink() ) ), '</a></h2>' );
			}
			?>
		</header>

		<div class="entry-excerpt" <?php hybrid_attr( 'entry-summary' ); ?>>
			<?php if ( has_post_format( 'quote' ) || has_post_format( 'aside' ) ) : ?>
				<?php the_content(""); ?>
			<?php elseif ( ! has_post_format( 'image' ) ) : ?>
				<?php the_excerpt(); ?>
			<?php endif; ?>
		</div>

		<div class="entry-footer clearfix">
			<div class="entry-meta">
				<?php postboard_entry_category(); ?>
				<?php postboard_posted_on(); ?>
				<?php postboard_entry_like(); ?>
			</div>
		</div>
		
	</article><!-- #post-## -->
</div><!-- .grid-item -->