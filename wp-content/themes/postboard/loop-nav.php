<?php if ( is_singular( 'post' ) ) : // If viewing a single post page. ?>

	<?php if ( postboard_mod( PREFIX . 'post-nav' ) ) : ?>

	<div class="post-nav">
		<?php if ( get_previous_post_link() ) : ?>
			<div class="nav-prev">
				<?php previous_post_link( 
					'%link',					
					'<div class="arrow"><i class="fa fa-angle-left"></i></div>'.
					'<div class="nav-text"><span>' . __( 'Previous Post', 'postboard' ) . '</span><h5>%title</h5></div>' 
				); ?>
			</div>
		<?php endif; ?>

		<?php if ( get_next_post_link() ) : ?>
			<div class="nav-next">
				<?php next_post_link(  
					'%link',
					'<div class="arrow"><i class="fa fa-angle-right"></i></div>' .
					'<div class="nav-text"><span>' . __( 'Next Post', 'postboard' ) . '</span><h5>%title</h5></div>'
					); 
				?>
			</div>
		<?php endif; ?>
	</div><!-- .loop-nav -->

	<?php endif; ?>

<?php elseif ( is_home() || is_archive() || is_search() ) : // If viewing the blog, an archive, or search results. ?>

	<?php $nav_type = postboard_mod( PREFIX . 'archive-nav' ); ?>

	<?php if ( $nav_type ) : ?>

		<?php if ( get_next_posts_link() ) : ?>

			<nav id="page-nav">
				<a href="<?php next_posts(); ?>">
					<img src="<?php echo esc_url( trailingslashit( get_template_directory_uri() ) . 'assets/img/bx_loader.gif' ); ?>" alt="<?php esc_attr_e( 'Loading...', 'postboard' ); ?>"/>
				</a>
			</nav>

		<?php endif; ?>

	<?php else : ?>

		<?php the_posts_navigation(); ?>

	<?php endif; ?>

<?php endif; // End check for type of page being viewed. ?>