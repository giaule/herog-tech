<?php get_header(); ?>

	<div id="primary" class="content-area">

		<?php if ( have_posts() ) : ?>

			<div class="intro">
				<?php
					the_archive_title( '<h3 class="section-heading">', '</h3>' );
					the_archive_description( '<p class="section-description">', '</p>' );
				?>

				<?php postboard_post_sorter(); ?>
			</div><!-- .intro -->

			<div class="content-loop grid clearfix">

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content' ); ?>

				<?php endwhile; ?>

			</div><!-- .content-loop -->

			<?php get_template_part( 'loop', 'nav' ); // Loads the loop-nav.php template ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

	</div><!-- #primary -->

	<?php get_sidebar(); ?>
<?php get_footer(); ?>