# Copyright (C) 2015 Theme Junkie
# This file is distributed under the GNU General Public License v2.0 or later.
msgid ""
msgstr ""
"Project-Id-Version: PostBoard 1.0.0\n"
"Report-Msgid-Bugs-To: http://www.theme-junkie.com/forum/\n"
"POT-Creation-Date: 2015-10-19 14:43:20+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-MO-DA HO:MI+ZONE\n"
"Last-Translator: Theme Junkie (support@theme-junkie.com)\n"
"Language-Team: Theme Junkie (support@theme-junkie.com)\n"
"X-Generator: grunt-wp-i18n 0.5.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: UNITED STATES\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-KeywordsList: "
"__;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c;_nc:4c,1,2;_"
"x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Textdomain-Support: yes\n"

#: 404.php:11
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:15
msgid ""
"It looks like nothing was found at this location. Maybe try one of the "
"links below or a search?"
msgstr ""

#: 404.php:23 page-templates/archive.php:39
msgid "Most Used Categories"
msgstr ""

#: 404.php:40 page-templates/archive.php:56
#. translators: %1$s: smiley
msgid "Try looking in the monthly archives. %1$s"
msgstr ""

#: admin/controls/switch.php:57
msgid "On"
msgstr ""

#: admin/controls/switch.php:58
msgid "Off"
msgstr ""

#: admin/extensions/fonts.php:18
msgid "Text Fonts"
msgstr ""

#: admin/extensions/fonts.php:20
msgid "Google Fonts"
msgstr ""

#: admin/extensions/fonts.php:125
msgid "All"
msgstr ""

#: admin/extensions/fonts.php:126
msgid "Cyrillic"
msgstr ""

#: admin/extensions/fonts.php:127
msgid "Cyrillic Extended"
msgstr ""

#: admin/extensions/fonts.php:128
msgid "Devanagari"
msgstr ""

#: admin/extensions/fonts.php:129
msgid "Greek"
msgstr ""

#: admin/extensions/fonts.php:130
msgid "Greek Extended"
msgstr ""

#: admin/extensions/fonts.php:131
msgid "Khmer"
msgstr ""

#: admin/extensions/fonts.php:132
msgid "Latin"
msgstr ""

#: admin/extensions/fonts.php:133
msgid "Latin Extended"
msgstr ""

#: admin/extensions/fonts.php:134
msgid "Vietnamese"
msgstr ""

#: admin/functions.php:53 inc/customizer.php:332
msgid "Site Title"
msgstr ""

#: admin/functions.php:54
msgid "Site title will automatically disapear if you upload a logo."
msgstr ""

#: admin/functions.php:55
msgid "Background"
msgstr ""

#: admin/functions.php:90
msgid "Select a tag &hellip;"
msgstr ""

#: admin/functions.php:118
msgid "Select a category &hellip;"
msgstr ""

#: admin/functions.php:151
msgid "Documentation"
msgstr ""

#: comments.php:31 comments.php:43
msgid "Comment navigation"
msgstr ""

#: comments.php:32 comments.php:44
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:33 comments.php:45
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:53
msgid "Comments are closed."
msgstr ""

#: comments.php:62
msgid "Required fields are marked %s"
msgstr ""

#: comments.php:67
msgid "Leave a Reply"
msgstr ""

#: comments.php:70
msgid "Your email address will not be published."
msgstr ""

#: content-none.php:6
msgid "Nothing Found"
msgstr ""

#: content-none.php:12
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: content-none.php:16
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: content-none.php:21
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: content-page.php:18 content-single.php:48 page-templates/archive.php:30
#: page-templates/teams.php:28
msgid "Pages:"
msgstr ""

#: content-page.php:24
msgid "Edit"
msgstr ""

#: functions.php:77
msgid "Primary Location"
msgstr ""

#: functions.php:78
msgid "Secondary Location"
msgstr ""

#: functions.php:106
msgid "1 Column Wide (Full Width)"
msgstr ""

#: functions.php:107
msgid "2 Columns: Content / Sidebar"
msgstr ""

#: functions.php:108
msgid "2 Columns: Sidebar / Content"
msgstr ""

#: functions.php:210
msgid "Primary Sidebar"
msgstr ""

#: functions.php:212
msgid "Main sidebar that appears on the right."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:323
msgid "Install Required Plugins"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:324
msgid "Install Plugins"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:325
msgid "Installing Plugin: %s"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:326
msgid "Something went wrong with the plugin API."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:327
msgid "This theme requires the following plugin: %1$s."
msgid_plural "This theme requires the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:332
msgid "This theme recommends the following plugin: %1$s."
msgid_plural "This theme recommends the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:337
msgid ""
"Sorry, but you do not have the correct permissions to install the %1$s "
"plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to install the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:342
msgid ""
"The following plugin needs to be updated to its latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgid_plural ""
"The following plugins need to be updated to their latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:347
msgid "There is an update available for: %1$s."
msgid_plural "There are updates available for the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:352
msgid ""
"Sorry, but you do not have the correct permissions to update the %1$s "
"plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to update the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:357
msgid "The following required plugin is currently inactive: %1$s."
msgid_plural "The following required plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:362
msgid "The following recommended plugin is currently inactive: %1$s."
msgid_plural "The following recommended plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:367
msgid ""
"Sorry, but you do not have the correct permissions to activate the %1$s "
"plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to activate the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:372
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:377
msgid "Begin updating plugin"
msgid_plural "Begin updating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:382
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:387
msgid "Return to Required Plugins Installer"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:388
msgid "Return to the dashboard"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:389
#: inc/classes/class-tgm-plugin-activation.php:3024
msgid "Plugin activated successfully."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:390
#: inc/classes/class-tgm-plugin-activation.php:2827
msgid "The following plugin was activated successfully:"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:391
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:392
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:393
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:394
msgid "Dismiss this notice"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:395
msgid "Please contact the administrator of this site for help."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:514
msgid "This plugin needs to be updated to be compatible with your theme."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:515
msgid "Update Required"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:629
msgid "Set the parent_slug config variable instead."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:823
#: inc/classes/class-tgm-plugin-activation.php:3433
msgid "Return to the Dashboard"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:930
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:930
#: inc/classes/class-tgm-plugin-activation.php:933
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:933
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2162
msgid "Required"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2165
msgid "Recommended"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2181
msgid "WordPress Repository"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2184
msgid "External Source"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2187
msgid "Pre-Packaged"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2204
msgid "Not Installed"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2208
msgid "Installed But Not Activated"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2210
msgid "Active"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2216
msgid "Required Update not Available"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2219
msgid "Requires Update"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2222
msgid "Update recommended"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2278
msgid "To Install <span class=\"count\">(%s)</span>"
msgid_plural "To Install <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:2281
msgid "Update Available <span class=\"count\">(%s)</span>"
msgid_plural "Update Available <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:2284
msgid "To Activate <span class=\"count\">(%s)</span>"
msgid_plural "To Activate <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:2374
msgid "Installed version:"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2382
msgid "Minimum required version:"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2394
msgid "Available version:"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2417
msgid ""
"No plugins to install, update or activate. <a href=\"%1$s\">Return to the "
"Dashboard</a>"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2431
msgid "Plugin"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2432
msgid "Source"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2433
msgid "Type"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2437
msgid "Version"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2438
msgid "Status"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2565
msgid "Upgrade message from the plugin author:"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2598
msgid "Install"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2604
msgid "Update"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2607
msgid "Activate"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2638
msgid "No plugins were selected to be installed. No action taken."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2640
msgid "No plugins were selected to be updated. No action taken."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2676
msgid "No plugins are available to be installed at this time."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2678
msgid "No plugins are available to be updated at this time."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2783
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2809
msgid "No plugins are available to be activated at this time."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3023
msgid "Plugin activation failed."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3357
msgid "Updating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3359
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3360
msgid "The installation of %1$s failed."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3364
msgid ""
"The installation and activation process is starting. This process may take "
"a while on some hosts, so please be patient."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3365
msgid "%1$s installed and activated successfully."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3365
#: inc/classes/class-tgm-plugin-activation.php:3371
msgid "Show Details"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3365
#: inc/classes/class-tgm-plugin-activation.php:3371
msgid "Hide Details"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3366
msgid "All installations and activations have been completed."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3367
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3370
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3371
msgid "%1$s installed successfully."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3372
msgid "All installations have been completed."
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:3373
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/customizer.php:38
msgid "General"
msgstr ""

#: inc/customizer.php:39
msgid "This panel is used for managing general section of your site."
msgstr ""

#: inc/customizer.php:48
msgid "RSS"
msgstr ""

#: inc/customizer.php:51
msgid "If you fill the custom rss url below, it will replace the default."
msgstr ""

#: inc/customizer.php:55
msgid "Custom RSS URL (eg. Feedburner)"
msgstr ""

#: inc/customizer.php:66 inc/template-tags.php:467 inc/template-tags.php:996
#: inc/widgets/widget-tabs.php:49
msgid "Comments"
msgstr ""

#: inc/customizer.php:72
msgid "Page Comment"
msgstr ""

#: inc/customizer.php:83
msgid "Post Date Style"
msgstr ""

#: inc/customizer.php:89
msgid "Style"
msgstr ""

#: inc/customizer.php:94
msgid "Absolute (June 20, 2015 10:19 am)"
msgstr ""

#: inc/customizer.php:95
msgid "Relative (1 week ago)"
msgstr ""

#: inc/customizer.php:104
msgid "Social"
msgstr ""

#: inc/customizer.php:110
msgid "Twitter URL"
msgstr ""

#: inc/customizer.php:117
msgid "Facebook URL"
msgstr ""

#: inc/customizer.php:124
msgid "Google Plus URL"
msgstr ""

#: inc/customizer.php:131
msgid "Instagram URL"
msgstr ""

#: inc/customizer.php:138
msgid "Pinterest URL"
msgstr ""

#: inc/customizer.php:145
msgid "LinkedIn URL"
msgstr ""

#: inc/customizer.php:152
msgid "Tumblr URL"
msgstr ""

#: inc/customizer.php:159
msgid "RSS URL"
msgstr ""

#: inc/customizer.php:170
msgid "Footer Text"
msgstr ""

#: inc/customizer.php:178
msgid "Customize the footer text."
msgstr ""

#: inc/customizer.php:181
msgid "&copy; %1$s %2$s &middot; %3$s %4$s &middot; Designed by %5$s"
msgstr ""

#: inc/customizer.php:194 inc/customizer.php:318 inc/customizer.php:595
msgid "Header"
msgstr ""

#: inc/customizer.php:195
msgid "This panel is used for managing header section of your site."
msgstr ""

#: inc/customizer.php:204
msgid "Logo"
msgstr ""

#: inc/customizer.php:210
msgid "Regular Logo"
msgstr ""

#: inc/customizer.php:221 inc/customizer.php:228
msgid "Header Style"
msgstr ""

#: inc/customizer.php:222
msgid "Choose header style"
msgstr ""

#: inc/customizer.php:233
msgid "Center"
msgstr ""

#: inc/customizer.php:234
msgid "With Ads"
msgstr ""

#: inc/customizer.php:243 inc/customizer.php:250
msgid "Secondary Menu Style"
msgstr ""

#: inc/customizer.php:244
msgid "Choose secondary menu style"
msgstr ""

#: inc/customizer.php:255
msgid "With Search Form"
msgstr ""

#: inc/customizer.php:256
msgid "Center without Search Form"
msgstr ""

#: inc/customizer.php:265 searchform.php:3 searchform.php:4
msgid "Search"
msgstr ""

#: inc/customizer.php:266
msgid "Show search icon and form"
msgstr ""

#: inc/customizer.php:283 inc/customizer.php:290
#: inc/widgets/widget-facebook.php:128 inc/widgets/widget-popular.php:149
#: inc/widgets/widget-random.php:124 inc/widgets/widget-recent.php:149
#: inc/widgets/widget-social.php:125 inc/widgets/widget-views.php:133
msgid "Title"
msgstr ""

#: inc/customizer.php:284
msgid "Display Title and Subtitle"
msgstr ""

#: inc/customizer.php:293
msgid "What's New?"
msgstr ""

#: inc/customizer.php:297
msgid "Subtitle/Description"
msgstr ""

#: inc/customizer.php:300
msgid ""
"Browse our latest posts below or <a href=\"#\">subscribe</a> to our "
"newsletter"
msgstr ""

#: inc/customizer.php:308
msgid "Color"
msgstr ""

#: inc/customizer.php:309
msgid "This panel is used for managing colors of your site."
msgstr ""

#: inc/customizer.php:344
msgid "Typography"
msgstr ""

#: inc/customizer.php:345
msgid "This panel is used for managing typography of your site."
msgstr ""

#: inc/customizer.php:355
msgid "Global"
msgstr ""

#: inc/customizer.php:361
msgid "Text font"
msgstr ""

#: inc/customizer.php:369
msgid "Heading font"
msgstr ""

#: inc/customizer.php:381
msgid "Layouts"
msgstr ""

#: inc/customizer.php:382
msgid ""
"This panel is used for managing several custom features/layouts of your "
"site."
msgstr ""

#: inc/customizer.php:391
msgid "Featured Posts"
msgstr ""

#: inc/customizer.php:397
msgid "Show featured posts"
msgstr ""

#: inc/customizer.php:404
msgid "Select a tag"
msgstr ""

#: inc/customizer.php:411
msgid "Number of posts"
msgstr ""

#: inc/customizer.php:418
msgid "Order by"
msgstr ""

#: inc/customizer.php:423
msgid "Date"
msgstr ""

#: inc/customizer.php:424 inc/template-tags.php:998
msgid "Random"
msgstr ""

#: inc/customizer.php:433
msgid "Archive"
msgstr ""

#: inc/customizer.php:434
msgid ""
"Applied to index/archive pages including category, tag, author and search "
"pages"
msgstr ""

#: inc/customizer.php:440
msgid "Archive Layout"
msgstr ""

#: inc/customizer.php:445
msgid "4 Columns"
msgstr ""

#: inc/customizer.php:446
msgid "3 Columns"
msgstr ""

#: inc/customizer.php:447
msgid "2 Columns"
msgstr ""

#: inc/customizer.php:448
msgid "1 Column"
msgstr ""

#: inc/customizer.php:453 inc/metabox.php:36
msgid "Grid Width"
msgstr ""

#: inc/customizer.php:454
msgid "Allow custom width per post"
msgstr ""

#: inc/customizer.php:459
msgid "Default"
msgstr ""

#: inc/customizer.php:460
msgid "Custom"
msgstr ""

#: inc/customizer.php:465
msgid "Use Infinite Scroll"
msgstr ""

#: inc/customizer.php:476 inc/customizer.php:630
msgid "Posts"
msgstr ""

#: inc/customizer.php:477
msgid "Posts is a single post page."
msgstr ""

#: inc/customizer.php:483
msgid "Post Header"
msgstr ""

#: inc/customizer.php:489
msgid "Show featured image"
msgstr ""

#: inc/customizer.php:496
msgid "Show post date"
msgstr ""

#: inc/customizer.php:503
msgid "Show post author name"
msgstr ""

#: inc/customizer.php:510
msgid "Show post like"
msgstr ""

#: inc/customizer.php:517
msgid "Show comment count"
msgstr ""

#: inc/customizer.php:524
msgid "Post Footer"
msgstr ""

#: inc/customizer.php:530
msgid "Show post categories"
msgstr ""

#: inc/customizer.php:537
msgid "Show post tags"
msgstr ""

#: inc/customizer.php:544
msgid "Show post share"
msgstr ""

#: inc/customizer.php:551
msgid "Show post navigation"
msgstr ""

#: inc/customizer.php:558
msgid "Show author box and related posts"
msgstr ""

#: inc/customizer.php:569
msgid "Page"
msgstr ""

#: inc/customizer.php:575
msgid "Show page title"
msgstr ""

#: inc/customizer.php:586 inc/widgets/widget-ads.php:84
#: inc/widgets/widget-ads125.php:99
msgid "Advertisement"
msgstr ""

#: inc/customizer.php:601 inc/customizer.php:643 inc/customizer.php:674
msgid "Ads Image"
msgstr ""

#: inc/customizer.php:602 inc/customizer.php:644 inc/customizer.php:675
msgid "Upload your ads image then put the url in the setting below."
msgstr ""

#: inc/customizer.php:609 inc/customizer.php:651 inc/customizer.php:682
msgid "Ads URL"
msgstr ""

#: inc/customizer.php:610 inc/customizer.php:652 inc/customizer.php:683
msgid "Put the ads url in the box below."
msgstr ""

#: inc/customizer.php:617 inc/customizer.php:659 inc/customizer.php:690
msgid "Or"
msgstr ""

#: inc/customizer.php:618 inc/customizer.php:660 inc/customizer.php:691
msgid "Pur you custom ads code (eg. adsense) in the box below."
msgstr ""

#: inc/customizer.php:631
msgid "Single post advertisement"
msgstr ""

#: inc/customizer.php:637
msgid "Ads Before Content"
msgstr ""

#: inc/customizer.php:668
msgid "Ads After Content"
msgstr ""

#: inc/extras.php:103 inc/hybrid/breadcrumb-trail.php:252
#. Translators: %s is the page number.
msgid "Page %s"
msgstr ""

#: inc/extras.php:254
msgid "Name"
msgstr ""

#: inc/extras.php:256
msgid "Email"
msgstr ""

#: inc/extras.php:258
msgid "Website"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:244
msgid "Browse:"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:246
msgid "Home"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:247
msgid "404 Not Found"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:248
msgid "Archives"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:250
#. Translators: %s is the search query. The HTML entities are opening and
#. closing curly quotes.
msgid "Search results for &#8220;%s&#8221;"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:254
#. Translators: Minute archive title. %s is the minute time format.
msgid "Minute %s"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:256
#. Translators: Weekly archive title. %s is the week date format.
msgid "Week %s"
msgstr ""

#: inc/hybrid/theme-layouts.php:458 inc/hybrid/theme-layouts.php:565
#: inc/hybrid/theme-layouts.php:650
msgid "Layout"
msgstr ""

#: inc/hybrid/theme-layouts.php:683
msgid "Global Layout"
msgstr ""

#: inc/megamenus/init.php:56
msgid "Mega Menus?"
msgstr ""

#: inc/metabox.php:61
msgid "How this post be displayed in archive grid"
msgstr ""

#: inc/metabox.php:68
msgid "Wide"
msgstr ""

#: inc/rating.php:86
msgid "Like this"
msgstr ""

#: inc/rating.php:91 inc/scripts.php:57
msgid "You already like this"
msgstr ""

#: inc/scripts.php:54
msgid "No more pages to load."
msgstr ""

#: inc/template-tags.php:116 inc/template-tags.php:146
msgid "%s ago"
msgstr ""

#: inc/template-tags.php:140
msgid "Posted on"
msgstr ""

#: inc/template-tags.php:219
msgid "Share:"
msgstr ""

#: inc/template-tags.php:270
msgid "Written by"
msgstr ""

#: inc/template-tags.php:338
msgid "Related Post"
msgstr ""

#: inc/template-tags.php:378
msgid "Pingback:"
msgstr ""

#: inc/template-tags.php:378
msgid "(Edit)"
msgstr ""

#: inc/template-tags.php:404
#. translators: 1: date, 2: time
msgid "%1$s at %2$s"
msgstr ""

#: inc/template-tags.php:405
msgid "%1$s&middot; Edit%2$s"
msgstr ""

#: inc/template-tags.php:405
msgid "Edit Comment"
msgstr ""

#: inc/template-tags.php:412
msgid "Your comment is awaiting moderation."
msgstr ""

#: inc/template-tags.php:416
msgid "Reply"
msgstr ""

#: inc/template-tags.php:444
msgid "Author"
msgstr ""

#: inc/template-tags.php:661
msgid "(Untitled)"
msgstr ""

#: inc/template-tags.php:990
msgid "Sort by:"
msgstr ""

#: inc/template-tags.php:992 inc/widgets/widget-tabs.php:48
msgid "Latest"
msgstr ""

#: inc/template-tags.php:994
msgid "Likes"
msgstr ""

#: inc/widgets/widget-ads.php:23 inc/widgets/widget-ads125.php:23
msgid "Easily to display any type of ad."
msgstr ""

#: inc/widgets/widget-ads.php:29
msgid "&raquo; Advertisement"
msgstr ""

#: inc/widgets/widget-ads.php:93 inc/widgets/widget-ads125.php:111
#: inc/widgets/widget-feedburner.php:105 inc/widgets/widget-newsletter.php:117
#: inc/widgets/widget-twitter.php:178 inc/widgets/widget-video.php:98
msgid "Title:"
msgstr ""

#: inc/widgets/widget-ads.php:100
msgid "Ad Code:"
msgstr ""

#: inc/widgets/widget-ads125.php:29
msgid "&raquo; Advertisement 125x125"
msgstr ""

#: inc/widgets/widget-ads125.php:118
msgid "Ad Code 1:"
msgstr ""

#: inc/widgets/widget-ads125.php:125
msgid "Ad Code 2:"
msgstr ""

#: inc/widgets/widget-ads125.php:132
msgid "Ad Code 3:"
msgstr ""

#: inc/widgets/widget-ads125.php:139
msgid "Ad Code 4:"
msgstr ""

#: inc/widgets/widget-facebook.php:28
msgid "&raquo; Facebook Like Box"
msgstr ""

#: inc/widgets/widget-facebook.php:31
msgid "Display a Facebook Like Box to connect visitors to your Facebook Page"
msgstr ""

#: inc/widgets/widget-facebook.php:45
msgid ""
"It looks like your Facebook URL is incorrectly configured. Please check it "
"in your <a href=\"%s\">widget settings</a>."
msgstr ""

#: inc/widgets/widget-facebook.php:135
msgid "Facebook Page URL"
msgstr ""

#: inc/widgets/widget-facebook.php:138
msgid ""
"The Like Box only works with <a "
"href=\"http://www.facebook.com/help/?faq=174987089221178\">Facebook "
"Pages</a>."
msgstr ""

#: inc/widgets/widget-facebook.php:144
msgid "Width"
msgstr ""

#: inc/widgets/widget-facebook.php:151
msgid "Height"
msgstr ""

#: inc/widgets/widget-facebook.php:158
msgid "Color Scheme"
msgstr ""

#: inc/widgets/widget-facebook.php:160 inc/widgets/widget-twitter.php:235
msgid "Light"
msgstr ""

#: inc/widgets/widget-facebook.php:161 inc/widgets/widget-twitter.php:236
msgid "Dark"
msgstr ""

#: inc/widgets/widget-facebook.php:169
msgid "Show Faces"
msgstr ""

#: inc/widgets/widget-facebook.php:171
msgid "Show profile photos in the plugin."
msgstr ""

#: inc/widgets/widget-facebook.php:178
msgid "Show Stream"
msgstr ""

#: inc/widgets/widget-facebook.php:180
msgid "Show the profile stream for the public profile."
msgstr ""

#: inc/widgets/widget-facebook.php:187
msgid "Show Border"
msgstr ""

#: inc/widgets/widget-facebook.php:189
msgid "Show a border around the plugin."
msgstr ""

#: inc/widgets/widget-facebook.php:196
msgid "Show Wall"
msgstr ""

#: inc/widgets/widget-facebook.php:198
msgid "Show the wall for a Places page rather than friend activity."
msgstr ""

#: inc/widgets/widget-feedburner.php:23
msgid "FeedBurner email subscription."
msgstr ""

#: inc/widgets/widget-feedburner.php:29
msgid "&raquo; Feedburner"
msgstr ""

#: inc/widgets/widget-feedburner.php:56
msgid "Enter your email"
msgstr ""

#: inc/widgets/widget-feedburner.php:60
msgid "Signup"
msgstr ""

#: inc/widgets/widget-feedburner.php:95
msgid "Get Updates"
msgstr ""

#: inc/widgets/widget-feedburner.php:96
msgid "Subscribe to our newsletter to receive breaking news by email."
msgstr ""

#: inc/widgets/widget-feedburner.php:112 inc/widgets/widget-newsletter.php:131
msgid "HTML or Text before form:"
msgstr ""

#: inc/widgets/widget-feedburner.php:119 inc/widgets/widget-newsletter.php:138
msgid "Feedburner ID:"
msgstr ""

#: inc/widgets/widget-newsletter.php:23
msgid "FeedBurner email subscription and Social icons."
msgstr ""

#: inc/widgets/widget-newsletter.php:29
msgid "&raquo; Subscribe &amp; Follow"
msgstr ""

#: inc/widgets/widget-newsletter.php:63
msgid "Your email address"
msgstr ""

#: inc/widgets/widget-newsletter.php:105
msgid "Subscribe &amp; Follow"
msgstr ""

#: inc/widgets/widget-newsletter.php:107
msgid "Subscribe to our newsletter"
msgstr ""

#: inc/widgets/widget-newsletter.php:109
msgid "Subscribe"
msgstr ""

#: inc/widgets/widget-newsletter.php:125
msgid "Display social icons?"
msgstr ""

#: inc/widgets/widget-newsletter.php:145
msgid "Button Text"
msgstr ""

#: inc/widgets/widget-popular.php:23
msgid "Display popular posts by comments with thumbnails."
msgstr ""

#: inc/widgets/widget-popular.php:29
msgid "&raquo; Popular Posts Thumbnails"
msgstr ""

#: inc/widgets/widget-popular.php:139
msgid "Popular Posts"
msgstr ""

#: inc/widgets/widget-popular.php:156 inc/widgets/widget-random.php:131
#: inc/widgets/widget-recent.php:156 inc/widgets/widget-views.php:140
msgid "Number of posts to show"
msgstr ""

#: inc/widgets/widget-popular.php:164 inc/widgets/widget-random.php:139
#: inc/widgets/widget-recent.php:164 inc/widgets/widget-views.php:148
msgid "Display post date?"
msgstr ""

#: inc/widgets/widget-random.php:23
msgid "Display random posts with thumbnails."
msgstr ""

#: inc/widgets/widget-random.php:29
msgid "&raquo; Random Posts Thumbnails"
msgstr ""

#: inc/widgets/widget-random.php:114
msgid "Random Posts"
msgstr ""

#: inc/widgets/widget-recent.php:23
msgid "Display recent posts with thumbnails."
msgstr ""

#: inc/widgets/widget-recent.php:29
msgid "&raquo; Recent Posts Thumbnails"
msgstr ""

#: inc/widgets/widget-recent.php:139
msgid "Recent Posts"
msgstr ""

#: inc/widgets/widget-social.php:23
msgid "Display your social media icons."
msgstr ""

#: inc/widgets/widget-social.php:29
msgid "&raquo; Social Icons"
msgstr ""

#: inc/widgets/widget-social.php:110
msgid "Follow us"
msgstr ""

#: inc/widgets/widget-social.php:132
msgid "Facebook"
msgstr ""

#: inc/widgets/widget-social.php:139
msgid "Twitter"
msgstr ""

#: inc/widgets/widget-social.php:146
msgid "Google Plus"
msgstr ""

#: inc/widgets/widget-social.php:153
msgid "Pinterest"
msgstr ""

#: inc/widgets/widget-social.php:160
msgid "LinkedIn"
msgstr ""

#: inc/widgets/widget-social.php:167
msgid "Instagram"
msgstr ""

#: inc/widgets/widget-social.php:174
msgid "RSS Feed"
msgstr ""

#: inc/widgets/widget-tabs.php:23
msgid "Display popular posts, recent posts, recent comments and tags in tabs."
msgstr ""

#: inc/widgets/widget-tabs.php:29
msgid "&raquo; Tabs"
msgstr ""

#: inc/widgets/widget-tabs.php:47
msgid "Popular"
msgstr ""

#: inc/widgets/widget-tabs.php:50
msgid "Tags"
msgstr ""

#: inc/widgets/widget-tabs.php:124
msgid "Number of Popular Posts"
msgstr ""

#: inc/widgets/widget-tabs.php:131
msgid "Number of Recent Posts"
msgstr ""

#: inc/widgets/widget-tabs.php:138
msgid "Number of Recent Comments"
msgstr ""

#: inc/widgets/widget-twitter.php:21
msgid "&raquo; Twitter Timeline"
msgstr ""

#: inc/widgets/widget-twitter.php:24
msgid "Display an official Twitter Embedded Timeline widget."
msgstr ""

#: inc/widgets/widget-twitter.php:90
msgid "My Tweets"
msgstr ""

#: inc/widgets/widget-twitter.php:163
msgid "Follow me on Twitter"
msgstr ""

#: inc/widgets/widget-twitter.php:183
msgid "Width (px):"
msgstr ""

#: inc/widgets/widget-twitter.php:188
msgid "Height (px):"
msgstr ""

#: inc/widgets/widget-twitter.php:193
msgid "# of Tweets Shown:"
msgstr ""

#: inc/widgets/widget-twitter.php:201
msgid ""
"You need to <a href=\"%1$s\" target=\"_blank\">create a widget at "
"Twitter.com</a>, and then enter your widget id (the long number found in "
"the URL of your widget's config page) in the field below. <a href=\"%2$s\" "
"target=\"_blank\">Read more</a>."
msgstr ""

#: inc/widgets/widget-twitter.php:209
msgid "Widget ID:"
msgstr ""

#: inc/widgets/widget-twitter.php:214
msgid "Layout Options:"
msgstr ""

#: inc/widgets/widget-twitter.php:215
msgid "No Header"
msgstr ""

#: inc/widgets/widget-twitter.php:216
msgid "No Footer"
msgstr ""

#: inc/widgets/widget-twitter.php:217
msgid "No Borders"
msgstr ""

#: inc/widgets/widget-twitter.php:218
msgid "No Scrollbar"
msgstr ""

#: inc/widgets/widget-twitter.php:219
msgid "Transparent Background"
msgstr ""

#: inc/widgets/widget-twitter.php:223
msgid "Link Color (hex):"
msgstr ""

#: inc/widgets/widget-twitter.php:228
msgid "Border Color (hex):"
msgstr ""

#: inc/widgets/widget-twitter.php:233
msgid "Timeline Theme:"
msgstr ""

#: inc/widgets/widget-video.php:23
msgid "Easily to display any type of video."
msgstr ""

#: inc/widgets/widget-video.php:29
msgid "&raquo; Video"
msgstr ""

#: inc/widgets/widget-video.php:88
msgid "Video"
msgstr ""

#: inc/widgets/widget-video.php:105
msgid "Video URL"
msgstr ""

#: inc/widgets/widget-video.php:112
msgid "Embed Code:"
msgstr ""

#: inc/widgets/widget-views.php:23
msgid "Display the most views posts with thumbnails."
msgstr ""

#: inc/widgets/widget-views.php:29
msgid "&raquo; Most Views Posts Thumbnails"
msgstr ""

#: inc/widgets/widget-views.php:79
msgid " Views"
msgstr ""

#: inc/widgets/widget-views.php:122
msgid "Most Views Posts"
msgstr ""

#: inc/widgets/widget-views.php:155
msgid "Display post view?"
msgstr ""

#: loop-nav.php:11
msgid "Previous Post"
msgstr ""

#: loop-nav.php:21
msgid "Next Post"
msgstr ""

#: loop-nav.php:40
msgid "Loading..."
msgstr ""

#: menu-secondary.php:28
msgid "Search for &hellip;"
msgstr ""

#: page-templates/teams.php:70
msgid ""
"Please install %1$sTheme Junkie Team Content%2$s plugin to use this page "
"template."
msgstr ""

#: search.php:8
msgid "Search Results for: %s"
msgstr ""

#. Theme Name of the plugin/theme
msgid "PostBoard"
msgstr ""

#. Theme URI of the plugin/theme
msgid "http://www.theme-junkie.com/themes/postboard"
msgstr ""

#. Description of the plugin/theme
msgid "A Grid Based WordPress Theme"
msgstr ""

#. Author of the plugin/theme
msgid "Theme Junkie"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.theme-junkie.com/"
msgstr ""

#. Template Name of the plugin/theme
msgid "Archive template"
msgstr ""

#. Template Name of the plugin/theme
msgid "Teams template"
msgstr ""

#: admin/extensions/fonts.php:199
msgctxt "font style"
msgid "Serif"
msgstr ""

#: admin/extensions/fonts.php:203
msgctxt "font style"
msgid "Sans Serif"
msgstr ""

#: admin/extensions/fonts.php:207
msgctxt "font style"
msgid "Monospaced"
msgstr ""

#: comments.php:23
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: functions.php:236
#. Translators: If there are characters in your language that are not supported
#. by Roboto, translate this to 'off'. Do not translate into your own language.
msgctxt "Roboto font: on or off"
msgid "on"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:1111
#: inc/classes/class-tgm-plugin-activation.php:2823
msgctxt "plugin A *and* plugin B"
msgid "and"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:1876
msgctxt "%s = version number"
msgid "TGMPA v%s"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2230
msgctxt "%1$s = install status, %2$s = update status"
msgid "%1$s, %2$s"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2275
msgctxt "plugins"
msgid "All <span class=\"count\">(%s)</span>"
msgid_plural "All <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/classes/class-tgm-plugin-activation.php:2366
msgctxt "as in: \"version nr unknown\""
msgid "unknown"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2486
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Install %2$s"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2490
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Update %2$s"
msgstr ""

#: inc/classes/class-tgm-plugin-activation.php:2495
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Activate %2$s"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:245
msgctxt "breadcrumbs aria label"
msgid "Breadcrumbs"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:708
msgctxt "minute and hour archives time format"
msgid "g:i a"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:725
msgctxt "minute archives time format"
msgid "i"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:742
msgctxt "hour archives time format"
msgid "g a"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:758 inc/hybrid/breadcrumb-trail.php:787
#: inc/hybrid/breadcrumb-trail.php:814 inc/hybrid/breadcrumb-trail.php:841
#: inc/hybrid/breadcrumb-trail.php:1176
msgctxt "yearly archives date format"
msgid "Y"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:759 inc/hybrid/breadcrumb-trail.php:815
#: inc/hybrid/breadcrumb-trail.php:1180
msgctxt "monthly archives date format"
msgid "F"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:760 inc/hybrid/breadcrumb-trail.php:1184
msgctxt "daily archives date format"
msgid "j"
msgstr ""

#: inc/hybrid/breadcrumb-trail.php:788
msgctxt "weekly archives date format"
msgid "W"
msgstr ""

#: inc/hybrid/theme-layouts.php:363
#. Translators: Default theme layout option.
msgctxt "theme layout"
msgid "Default"
msgstr ""

#: sidebar.php:13
msgctxt "Sidebar aria label"
msgid "Primary Sidebar"
msgstr ""