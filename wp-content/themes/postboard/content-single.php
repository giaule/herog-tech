<?php
/*
// Get the data set in customizer
$cat        = postboard_mod( PREFIX . 'post-cat' );
$tag        = postboard_mod( PREFIX . 'post-tag' );
*/
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php hybrid_attr( 'post' ); ?>>

	<?php if ( has_post_format( 'video' ) ) : ?>
		<div class="entry-image video">
			<?php echo hybrid_media_grabber( array( 'type' => 'video', 'split_media' => true ) ); ?>
		</div>
	<?php elseif ( has_post_format( 'audio' ) ) : ?>
		<div class="entry-image audio">
			<?php echo hybrid_media_grabber( array( 'type' => 'audio', 'split_media' => true ) ); ?>
		</div>
	<?php elseif ( has_post_format( 'image' ) )	: // do nothing ?>
	<?php elseif ( has_post_format( 'gallery' ) ) : ?>
		<?php echo postboard_get_format_gallery(); // Get the gallery ?>
	<?php else : ?>
		<?php postboard_featured_image(); ?>
	<?php endif; ?>

	<header class="entry-header wrap">

		<?php the_title( '<h1 class="entry-title" ' . hybrid_get_attr( 'entry-title' ) . '>', '</h1>' ); ?>

		<div class="entry-meta clearfix">

			<?php postboard_posted_on_single(); ?>

			<?php postboard_entry_author(); ?>

			<?php postboard_entry_like(); ?>

			<?php postboard_comment_count(); ?>

		</div><!-- .entry-meta -->
		
	</header>

	<div class="entry-content wrap clearfix" <?php hybrid_attr( 'entry-content' ); ?>>

		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'postboard' ),
				'after'  => '</div>',
			) );
		?>
	
	</div>

	<footer class="entry-footer wrap clearfix">

		<?php postboard_entry_category(); ?>

		<?php postboard_entry_tags(); ?>

		<?php postboard_entry_share(); ?>
		
	</footer>
	
	<?php if ( function_exists( 'sharing_display' ) ) : ?>
		<div class="jetpack-share-like">
			<?php sharing_display( '', true ); ?>
			<?php if ( class_exists( 'Jetpack_Likes' ) ) { $custom_likes = new Jetpack_Likes; echo $custom_likes->post_likes( '' ); } ?>
		</div>
	<?php endif; ?>
	
</article><!-- #post-## -->
